// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2016-02-08 using
// generator-karma 1.0.0

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    // as well as any additional frameworks (requirejs/chai/sinon/...)
    frameworks: [
      "jasmine"
    ],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'bower_components/jquery/dist/jquery.js',
      'bower_components/jquery-ui/jquery-ui.js',
      'bower_components/bootstrap/dist/js/bootstrap.min.js',
      'bower_components/metisMenu/dist/metisMenu.js',
      'bower_components/slimScroll/jquery.slimscroll.js',
      'bower_components/angular/angular.js',
      'bower_components/angular-ui-router/release/angular-ui-router.js',
      'bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      'bower_components/angular-animate/angular-animate.js',
      'bower_components/angular-resource/angular-resource.js',
      'bower_components/angular-sanitize/angular-sanitize.js',
      'bower_components/angular-filter/dist/angular-filter.js',
      'bower_components/angular-i18n/angular-locale_es-mx.js',
      'bower_components/moment/moment.js',
      'bower_components/moment/locale/es.js',
      'bower_components/angular-fullscreen/src/angular-fullscreen.js',
      'bower_components/jszip/dist/jszip.js',
      'bower_components/pdfmake/build/pdfmake.js',
      'bower_components/pdfmake/build/vfs_fonts.js',
      'bower_components/datatables.net/js/jquery.dataTables.js',
      'bower_components/datatables.net-bs/js/dataTables.bootstrap.js',
      'bower_components/datatables.net-buttons/js/dataTables.buttons.js',
      'bower_components/datatables.net-buttons/js/buttons.colVis.js',
      'bower_components/datatables.net-buttons/js/buttons.html5.js',
      'bower_components/datatables.net-buttons/js/buttons.print.js',
      'bower_components/datatables.net-buttons-bs/js/buttons.bootstrap.js',
      'bower_components/angular-datatables/dist/angular-datatables.js',
      'bower_components/angular-datatables/dist/plugins/buttons/angular-datatables.buttons.js',
      'bower_components/blueimp-gallery/js/jquery.blueimp-gallery.min.js',
      'bower_components/blueimp-gallery/js/blueimp-gallery-fullscreen.js',
      'bower_components/blueimp-gallery-bootstrap/js/bootstrap-image-gallery.js',
      'bower_components/blueimp-canvas-to-blob/js/canvas-to-blob.js',
      'bower_components/jquery.panzoom/dist/jquery.panzoom.min.js',
      'bower_components/angular-loading-bar/build/loading-bar.js',
      'bower_components/spin.js/spin.js',
      'bower_components/ladda/dist/ladda.min.js',
      'bower_components/angular-ladda/dist/angular-ladda.min.js',
      'bower_components/iCheck/icheck.min.js',
      'bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
      'bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.templates.js',
      'bower_components/angular-ui-tree/dist/angular-ui-tree.js',
      'bower_components/highcharts/highcharts.js',
      'bower_components/highcharts/highcharts-more.js',
      'bower_components/highcharts/modules/exporting.js',
      'bower_components/highcharts-ng/dist/highcharts-ng.js',
      'bower_components/angular-file-upload/dist/angular-file-upload.min.js',
      'bower_components/watch-dom/dist/watch-dom.js',
      'bower_components/transitionize/dist/transitionize.js',
      'bower_components/fastclick/lib/fastclick.js',
      'bower_components/switchery/dist/switchery.js',
      'bower_components/ng-switchery/src/ng-switchery.js',
      'bower_components/angular-busy/dist/angular-busy.js',
      'bower_components/sweetalert/dist/sweetalert.min.js',
      'bower_components/angular-mocks/angular-mocks.js',
      // endbower
      "app/scripts/**/*.js",
      "test/mock/**/*.js",
      "test/spec/**/*.js"
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      "PhantomJS"
    ],

    // Which plugins to enable
    plugins: [
      "karma-phantomjs-launcher",
      "karma-jasmine"
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
