function ResetPasswordController($scope, $location, $state, AuthService) {

	$scope.data = {
		'loading': false,
		'user': {
			'email': '',
			'password': '',
			'checkPassword': ''
		}
	}


	$scope.resetPassword = function(form, data) {

		if(form.$valid && data.password.length >= 6) {

			$scope.data.loading = true;

			AuthService
	            .resetPassword({
	                'token': $location.search().token,
	                'email': data.email,
	                'password': data.password,
	                'password_confirmation': data.checkPassword
	            })
	            .then(function(response) {

	            	var roles = response.data.response.roles;
	            	var isAdministrator = false;

                	$scope.data.loading = false;

	                swal({
		                title: 'Correcto!',
		                html: true,
		                text: 'La contraseña fue reestablecida con éxito.',   
		                type: 'success',    
		                confirmButtonColor: '#128f76',   
		                confirmButtonText: 'Continuar'
		            }, function(){

		            	for(var i in roles) {

		            		if(roles[i].name === 'Administrator') {

		            			isAdministrator = true;
		            			break;

		            		}

		            	}

		            	if(!isAdministrator) {

		            		window.location = 'http://www.aguagente.com';

		            	} else {

		            		$state.go('login');
		            		
		            	}

		            });

	            })

		} else {

			$scope.$broadcast('show-errors-check-validity');

            swal({
                title: 'Un momento',
                text: 'Favor de completar los datos obligatorios.',   
                type: 'error',    
                confirmButtonColor: '#128f76',   
                confirmButtonText: 'Aceptar'
            });

		}

	}

}

ResetPasswordController.$inject = ['$scope', '$location', '$state', 'AuthService'];