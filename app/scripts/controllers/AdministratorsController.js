/**
 * AdministratorsController
 *
 *
 * @param $scope
 * @param $modal
 * @param $timeout
 * @param DTOptionsBuilder
 * @param DTColumnDefBuilder
 * @param Users
 * @param administrators
 * @constructor
 */
function AdministratorsController(
    $scope, $modal, $timeout,
    DTOptionsBuilder, DTColumnDefBuilder, Users,
    administrators) {

	$scope.data = {
		'administrators' : administrators,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Usuarios administradores'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Usuarios administradores',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
	    ]
    }

    $scope.add = function() {

        $modal.open({

            templateUrl: 'views/administrators-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'AuthService',

                function($scope, $filter, $modalInstance, AuthService) {

                    var instance = this;

                    instance.data = {
                        'user': {
                            'name': '',
                            'email': '',
                            'password': '',
                            'checkPassword': ''
                        },
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, user) {

                        if(form.$valid && user.password === user.checkPassword && user.password.length >= 6) {

                            instance.data.loading = true;

                            AuthService
                                .register({
                                    'name': instance.data.user.name,
                                    'email': instance.data.user.email,
                                    'password': instance.data.user.password
                                })
                                .then(function(response) {

                                    instance.data.loading = false;
                                    $modalInstance.close(response);

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }
            ]
        })
        .result.then(function (administrator) {

            $scope.data.administrators.push(administrator);

        })
        .catch(function() {

        });

    }

    $scope.edit = function(administrator) {

        $modal.open({

            templateUrl: 'views/administrators-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'Users',

                function($scope, $filter, $modalInstance, Users) {

                    var instance = this;

                    administrator.password = '';
                    administrator.checkPassword = '';

                    instance.data = {
                        'user': administrator,
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, user) {

                        if(form.$valid && user.password === user.checkPassword && user.password.length >= 6) {

                            instance.data.loading = true;

                            Users
                                .update({
                                    'id': user.id,
                                    'name': user.name,
                                    'email': user.email,
                                    'password': user.password
                                })
                                .then(function(response) {

                                    swal({
                                        title: 'Cliente actualizado correctamente',
                                        text: '',
                                        type: 'success',
                                        confirmButtonColor: '#128f76',
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);

                                    });

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }
            ]
        })
        .result.then(function (administrator) {

            $scope.data.administrators.push(administrator);

        })
        .catch(function() {

        });

    }

}

AdministratorsController.$inject = [
    '$scope', '$modal', '$timeout',
    'DTOptionsBuilder', 'DTColumnDefBuilder', 'Users',
    'administrators'
];
