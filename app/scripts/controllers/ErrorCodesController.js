/**
 * ErrorCodesController
 *
 *
 * @param $scope
 * @param $modal
 * @param $timeout
 * @param DTOptionsBuilder
 * @param DTColumnDefBuilder
 * @param ErrorCodes
 * @param errorCodes
 * @constructor
 */
function ErrorCodesController(
	$scope, $modal, $timeout,
	DTOptionsBuilder, DTColumnDefBuilder, ErrorCodes,
	errorCodes) {

	$scope.data = {
		'errorCodes' : errorCodes,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Técnicos'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Técnicos',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
	    ]
    }

    $scope.add = function() {

        $modal.open({

            templateUrl: 'views/error-codes-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'ErrorCodes',

                function($scope, $filter, $modalInstance, ErrorCodes) {

                    var instance = this;

                    instance.data = {
                        'errorCode': {
                            'name': '',
                            'code': '',
                            'category': ''
                        },
                        'categories': [
                            'Basic Codes',
                            'Detailed Codes',
                            'Other'
                        ],
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, errorCode) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            ErrorCodes
                                .create({
                                    'name': errorCode.name,
                                    'code': errorCode.code,
                                    'category': errorCode.category
                                })
                                .then(function(response) {

                                    swal({
                                        title: 'Código de error creado correctamente',
                                        text: '',
                                        type: 'success',
                                        confirmButtonColor: '#128f76',
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);

                                    });

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }
            ]
        })
        .result.then(function (errorCode) {

            $scope.data.errorCodes.push(errorCode);

        })
        .catch(function() {

        });

    }

    $scope.edit = function(errorCode) {

        $modal.open({

            templateUrl: 'views/error-codes-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'ErrorCodes',

                function($scope, $filter, $modalInstance, ErrorCodes) {

                    var instance = this;

                    instance.data = {
                        'errorCode': angular.copy(errorCode),
                        'categories': [
                            'Basic Codes',
                            'Detailed Codes',
                            'Other'
                        ],
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, errorCode) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            ErrorCodes
                                .update(errorCode)
                                .then(function(response) {

                                    swal({
                                        title: 'Código de error actualizado correctamente',
                                        text: '',
                                        type: 'success',
                                        confirmButtonColor: '#128f76',
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);

                                    });

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }
            ]
        })
        .result.then(function (editedErrorCode) {

            $scope.data.errorCodes[$scope.data.errorCodes.indexOf(errorCode)] = editedErrorCode;

        })
        .catch(function() {

        });

    }

    $scope.delete = function(errorCode) {

        swal({
            title: '¿Estás seguro de eliminar el código de error?',
            text: '',
            type: 'warning',
            confirmButtonColor: '#128f76',
            confirmButtonText: 'Si, eliminar.',
            cancelButtonText: 'Cancelar',
            showCancelButton: true
        }, function() {

            ErrorCodes
                .delete(errorCode)
                .then(function () {

                    $scope.data.errorCodes.splice(
                        $scope.data.errorCodes.indexOf(errorCode),
                        1
                    );

                })

        });

    }

}
ErrorCodesController.$inject = [
	'$scope', '$modal', '$timeout',
	'DTOptionsBuilder', 'DTColumnDefBuilder', 'ErrorCodes',
	'errorCodes'
];
