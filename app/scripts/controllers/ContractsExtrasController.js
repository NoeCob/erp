function ContractsExtrasController(
	$scope, $timeout, $modal,
	DTOptionsBuilder, DTColumnDefBuilder,
	Categories, Elements,
	categories) {

	$scope.data = {
		'categories': categories,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: [0,1]
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf', 
	            	exportOptions: {
			          columns: [0,1]
			        },
			        text: 'PDF',
                    title: 'Categorias'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: [0,1]
			        },
			        text: 'Imprimir',
                    title: 'Contratos',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(3).notSortable(),
            DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
	    ]
	}

	$scope.addCategory = function() {

		$modal.open({

            templateUrl: 'views/contracts-extras-category.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$filter', '$modalInstance', 'Categories', 
            
                function($filter, $modalInstance, Categories) {

                    var instance = this;

                    instance.data = {
                        'category': {
                        	'name': '',
                        	'description': ''
                        },
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, category) {

                    	if(form.$valid) {

                    		instance.data.loading = true;

							Categories
	                    		.create(category)
	                    		.then(function(response) {

	                    			instance.data.loading = false;
	                    			$modalInstance.close(response);

	                    		}, function() {
	                    			instance.data.loading = false;
	                    		})

                    		
                    	} else {

				            swal({
				                title: 'Un momento',
				                text: "Favor de completar los datos obligatorios.",   
				                type: "error",    
				                confirmButtonColor: "#128f76",   
				                confirmButtonText: "Aceptar"
				            });
                    		
                    	}

                    }

                }
            ]
        })
        .result.then(function (category) {
            
            $scope.data.categories.push(category);

        })
        .catch(function() {
        
        });

	}

	$scope.editCategory = function(category) {

		$modal.open({

            templateUrl: 'views/contracts-extras-category.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 

            	category: function() {

            		return category;

            	}
            },
            controller: ['$filter', '$modalInstance', 'Categories', 'category', 
            
                function($filter, $modalInstance, Categories, category) {

                    var instance = this;

                    instance.data = {
                        'category': category,
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, category) {

                    	if(form.$valid) {

                    		instance.data.loading = true;
							Categories
	                    		.update(category)
	                    		.then(function(response) {

	                    			instance.data.loading = false;
	                    			$modalInstance.close(response);

	                    		}, function() {
	                    			instance.data.loading = false;
	                    		});

                    		
                    	} else {

				            swal({
				                title: 'Un momento',
				                text: "Favor de completar los datos obligatorios.",   
				                type: "error",    
				                confirmButtonColor: "#128f76",   
				                confirmButtonText: "Aceptar"
				            });
                    		
                    	}

                    }

                }
            ]
        })
        .result.then(function (categoryEdited) {

            $scope.data.categories[$scope.data.categories.indexOf(category)] = categoryEdited;

        })
        .catch(function() {
        
        });

	}

	$scope.deleteCategory = function(category) {

		swal({   
			title: "Estás seguro ?",   
			text: "La categoría y sus elementos ya no estarán disponibles",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, eliminar",   
			closeOnConfirm: false 
		}, function(){   

			category.loading = true;

			Categories
				.delete({
					'id_categories': category.id_categories
				})
				.then(function(response) {

					$scope
						.data
						.categories
						.splice(
							$scope.data.categories.indexOf(category), 
							1
						);

					swal("Eliminada!", "La categoría ha sido eliminada.", "success"); 
					category.loading = false;
				},function() {
        			category.loading = false;
        		})

		});
	
	}

	$scope.addElement = function(category) {

		$modal.open({

            templateUrl: 'views/contracts-extras-element.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 

                category: function() {

                    return angular.copy(category);

                },

                element: function() {

                	return {
                		'id_categories': category.id_categories,
                		'name': '',
                		'price': '',
                		'image': ''
                	};

                }, 

                image: function() {

                	return '';

                }

            },
            controller: ['$filter', '$modalInstance', 'Categories', 'Elements', 'category', 'element', 'image', 
            
                function($filter, $modalInstance, Categories, Elements, category, element, image) {

                    var instance = this;

                    instance.data = {
                        'category': category,
                        'element': element,
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.previewImage = function(elemento){

				        var oFReader = new FileReader();
				        oFReader.readAsDataURL(document.getElementById(elemento.id).files[0]);

				        oFReader.onload = function (oFREvent) {

				            var img = new Image();

				            img.onload = function(){
 
				                var canvas = document.getElementById('element-image');
				                var ctx = canvas.getContext("2d");
				                ctx.clearRect(0, 0, canvas.width, canvas.height);
				                canvas.width = img.width;
				                canvas.height = img.height;
				                ctx.drawImage(img, 0, 0, img.width, img.height);
				                
				            };

				            img.src = oFREvent.target.result;

				        };
				    
				    }

				    instance.removeImage = function(canvas_id) {
				        
				        var canvas = document.getElementById('element-image');
				        var ctx = canvas.getContext("2d");
				        ctx.clearRect(0, 0, canvas.width, canvas.height);

    				}

                    instance.save = function(form, element) {

                    	if(form.$valid) {

                    		instance.data.loading = true;

							Elements
	                    		.create(element)
	                    		.then(function(response) {

	                    			instance.data.loading = false;
	                    			$modalInstance.close(response);

	                    		}, function() {
	                    			instance.data.loading = false;
	                    		})

                    		
                    	} else {

                    		$scope.$broadcast('show-errors-check-validity');
				            swal({
				                title: 'Un momento',
				                text: "Favor de completar los datos obligatorios.",   
				                type: "error",    
				                confirmButtonColor: "#128f76",   
				                confirmButtonText: "Aceptar"
				            });
                    		
                    	}

                    }

                }
            ]
        })
        .result.then(function (element) {
            
            $scope.data.categories[$scope.data.categories.indexOf(category)].elements.push(element);

        })
        .catch(function() {
        
        });

	}

	$scope.editElement = function(category, element) {

		$modal.open({

            templateUrl: 'views/contracts-extras-element.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { 

                category: function() {

                    return angular.copy(category);

                },

                element: function() {

                	return angular.copy(element);

                },

                image: function() {

                	return element.image;
                }

            },
            controller: ['$filter', '$modalInstance', 'Categories', 'Elements', 'category', 'element', 'image', 
            
                function($filter, $modalInstance, Categories, Elements, category, element, image) {

                    var instance = this;

                    element.price = parseFloat(element.price);
                    element.image = image;

                    instance.data = {
                        'category': category,
                        'element': element,
                        'loading': false
                    }

                    var oFReader = new FileReader();
			        oFReader.readAsDataURL(element.image);

			        oFReader.onload = function (oFREvent) {

			            var img = new Image();

			            img.onload = function(){

			                var canvas = document.getElementById('element-image');
			                var ctx = canvas.getContext("2d");
			                ctx.clearRect(0, 0, canvas.width, canvas.height);
			                canvas.width = img.width;
			                canvas.height = img.height;
			                ctx.drawImage(img, 0, 0, img.width, img.height);
			                
			            };

			            img.src = oFREvent.target.result;

			        }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.previewImage = function(elemento){

				        var oFReader = new FileReader();
				        oFReader.readAsDataURL(document.getElementById(elemento.id).files[0]);

				        oFReader.onload = function (oFREvent) {

				            var img = new Image();

				            img.onload = function(){
 
				                var canvas = document.getElementById('element-image');
				                var ctx = canvas.getContext("2d");
				                ctx.clearRect(0, 0, canvas.width, canvas.height);
				                canvas.width = img.width;
				                canvas.height = img.height;
				                ctx.drawImage(img, 0, 0, img.width, img.height);
				                
				            };

				            img.src = oFREvent.target.result;

				        };
				    
				    }

				    instance.removeImage = function(canvas_id) {
				        
				        var canvas = document.getElementById('element-image');
				        var ctx = canvas.getContext("2d");
				        ctx.clearRect(0, 0, canvas.width, canvas.height);

    				}

                    instance.save = function(form, element) {

                    	if(form.$valid) {

                    		instance.data.loading = true;

							Elements
	                    		.update(element)
	                    		.then(function(response) {

	                    			instance.data.loading = false;
	                    			$modalInstance.close(response)

	                    		}, function() {
	                    			instance.data.loading = false;
	                    		})

                    		
                    	} else {

                    		$scope.$broadcast('show-errors-check-validity');

				            swal({
				                title: 'Un momento',
				                text: "Favor de completar los datos obligatorios.",   
				                type: "error",    
				                confirmButtonColor: "#128f76",   
				                confirmButtonText: "Aceptar"
				            });
                    		
                    	}

                    }

                }
            ]
        })
        .result.then(function (elementEdited) {
            
            $scope
            	.data
            	.categories[
            		$scope
            			.data
            			.categories
            			.indexOf(category)
    			].elements[
    				$scope
	            	.data
	            	.categories[
	            		$scope
	            			.data
	            			.categories
	            			.indexOf(category)
	    			].elements.indexOf(element)
    			] = elementEdited;

        })
        .catch(function() {
        
        });

	}

	$scope.deleteElement = function(category, element) {

		swal({   
			title: "Estás seguro ?",   
			text: "El elemento ya no estará disponible",   
			type: "warning",   
			showCancelButton: true,   
			confirmButtonColor: "#DD6B55",   
			confirmButtonText: "Si, eliminar",   
			closeOnConfirm: false 
		}, function(){   

			Elements
				.delete({
					'id_categories_elements': element.id_categories_elements
				})
				.then(function(response) {

					var categoryIndex = $scope
        			.data
        			.categories
        			.indexOf(category);

        			var elementIndex = $scope
        			.data
        			.categories[categoryIndex]
        			.elements
        			.indexOf(element);

					$scope
	            	.data
	            	.categories[categoryIndex]
	    			.elements
	    			.splice(
	    				elementIndex,
	    				1
	    			);

					swal("Eliminado!", "El elemento ha sido eliminado.", "success"); 
				})

		});
	
	}

	$scope.reviewImage = function(element) {
        
        console.log(element);
        element.loading = true;

		var oFReader = new FileReader();

        oFReader.readAsDataURL(element.image);

        oFReader.onload = function (oFREvent) {

	        $('#blueimp-gallery').data('use-bootstrap-modal', false);

	        $timeout(function(){

	            element.loading = false;
	            blueimp.Gallery( 
	                [{
	                	'href': 'data:image/png;base64,'+oFREvent.target.result.split(',')[1]
	                }],
	                $('#blueimp-gallery').data()
	            );
	            
	        }, 250);

        };

    }
}

ContractsExtrasController.$inject = [
	'$scope', '$timeout', '$modal',
	'DTOptionsBuilder', 'DTColumnDefBuilder',
	'Categories', 'Elements',
	'categories'
];