function DashboardController($scope, Dashboard) {

    $scope.dashboard = {
        'totals': {
            'income': 0,
            'sr': 0,
            'clients': 0,
            'debts': 0
        },
        'incomes_total': 0,
        'sr_total': 0,
        'incomes': [],
        'clients': [],
        'tickets': {
            'opened': 0,
            'assigned': 0,
            'completed': 0,
            'confirmed': 0,
            'closed': 0
        },
        'subscription_events': [],
        'general': [
            [
                0,0,0,0,0,0
            ]
        ],
        'views': []
    };
    $scope.suscriptionsChart = {
        options: {
            chart: {
                type: 'areaspline'
            }
        },
        series: [
            {
                data: [],
                name: 'Suscripciones'
            }
        ],
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: ''
        },
        size: {
            height: 300
        }
    };
    $scope.eventsChart = {
        options: {
            chart: {
                type: 'area'
            }
        },
        series: [
            {
                data: [],
                name: 'Cancelaciones',
                color: '#CF4647'
            }
        ],
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: ''
        },
        size: {
            height: 300
        }
    };
    $scope.incomesChart = {
        options: {
            chart: {
                type: 'areaspline'
            }
        },
        series: [
            {
                data: [],
                name: 'Ingresos'
            }
        ],
        title: {
            text: ''
        },
        xAxis: {
            type: 'datetime'
        },
        yAxis: {
            title: ''
        },
        size: {
            height: 300
        }
    };
    $scope.from = '';
    $scope.to = '';
    $scope.isFromOpened = false;
    $scope.isToOpened = false;
    $scope.getDashboard = function(from, to) {
        var params = {};
        if(from != '') params['from'] = from;
        if(to != '') params['to'] = to;
        Dashboard.get(params).then(function(dashboard) {
            $scope.dashboard.totals.income = dashboard.totals.income;
            $scope.dashboard.totals.debts = dashboard.totals.debts;
            $scope.dashboard.totals.sr = dashboard.totals.client_social + dashboard.totals.aguagente_social;
            
            dashboard.clients.forEach(function(client) { 
                if (client.status == 'accepted') {
                    $scope.dashboard.totals.clients = parseFloat(client.charged);
                }
            });
            
            $scope.dashboard.incomes_total = dashboard.incomes.reduce(function(a, b) { return a + parseFloat(b.amount); }, 0);
            $scope.dashboard.sr_total =  dashboard.incomes.reduce(function(a, b) { return a + parseFloat(b.client_social) + parseFloat(b.aguagente_social); }, 0);
            $scope.dashboard.incomes = dashboard.incomes;
            $scope.dashboard.clients = dashboard.clients;
            $scope.dashboard.tickets = dashboard.tickets;
            $scope.dashboard.general = dashboard.general;
            $scope.dashboard.views = dashboard.views;
            $scope.dashboard.subscription_events = dashboard.subscription_events;
            $scope.suscriptionsChart.series[0].data = dashboard.newSuscriptions.map(function (suscripcion) { 
                return [moment(suscripcion.month).unix() * 1000, parseInt(suscripcion.quantity)] 
            });
            $scope.incomesChart.series[0].data = dashboard.incomes.map(function(income) { return [moment(income.date).unix() * 1000, parseFloat(income.amount)] });
            $scope.eventsChart.series[0].data = dashboard.subscription_events
                .filter(function(event) { 
                    return event.type == 'subscription.canceled';
                })
                .map(function(event) { 
                    return [moment(event.month).unix() * 1000, parseInt(event.quantity)] 
                });
        });
    }
    $scope.getDashboard('', '');
    $scope.incomesPercent = function() {
        return Math.round($scope.dashboard.incomes_total / $scope.dashboard.totals.income * 100);
    }
    $scope.socialPercent = function() {
        return Math.round($scope.dashboard.sr_total / $scope.dashboard.totals.sr * 100);
    }
}


DashboardController.$inject = ['$scope', 'Dashboard'];
