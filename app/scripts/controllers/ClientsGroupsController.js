function ClientsGroupsController(
    $scope, $modal,
    DTOptionsBuilder, DTColumnDefBuilder, Clients, Groups,
    groups) {

	$scope.data = {
		'groups' : groups,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Grupos'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Grupos',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');
	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(6).notSortable()
	    ]

    }

    $scope.add = function() {

        $modal.open({

            templateUrl: 'views/clients-groups-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                groups: function() {
                    return groups;
                }
            },
            controller: ['$scope', '$filter', '$modalInstance', 'Groups', 'groups',

                function($scope, $filter, $modalInstance, Groups, groups) {

                    var instance = this;

                    instance.data = {
                        'group': {
                            'name': '',
                            'deposit': '',
                            'monthly_fee': '',
                            'installation_fee': '',
                            'trial_days': 0,
                            'trial_days_price': 0,
                            'sign_into': ''
                        },
                        'groups': groups,
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, group) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Groups
                                .create({
                                    'name': group.name,
                                    'deposit': group.deposit,
                                    'monthly_fee': group.monthly_fee,
                                    'installation_fee': group.installation_fee,
                                    'trial_days': group.trial_days,
                                    'trial_days_price': group.trial_days_price,
                                    'sign_into': group.sign_into.id_groups
                                })
                                .then(function(response) {

                                    instance.data.loading = false;

                                    $modalInstance.close(response);

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }
            ]
        })
        .result.then(function (group) {

            $scope.data.groups.push(group);

        })
        .catch(function() {

        });

    }

    $scope.edit = function(group) {

        $modal.open({

            templateUrl: 'views/clients-groups-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: {
                groups: function() {
                    return groups;
                },
                group: function() {
                    return group;
                }
            },
            controller: ['$scope', '$filter', '$modalInstance', 'Groups', 'groups', 'group',

                function($scope, $filter, $modalInstance, Groups, groups, group) {

                    var instance = this;

                    instance.data = {
                        'group': group,
                        'groups': groups,
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, group) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Groups
                                .update({
                                    'id_groups': group.id_groups,
                                    'name': group.name,
                                    'deposit': group.deposit,
                                    'monthly_fee': group.monthly_fee,
                                    'installation_fee': group.installation_fee,
                                    'trial_days': group.trial_days,
                                    'trial_days_price': group.trial_days_price,
                                    'sign_into': group.sign_into.id_groups
                                })
                                .then(function(response) {

                                    instance.data.loading = false;

                                    $modalInstance.close(response);

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }
            ]
        })
        .result.then(function (groupEdited) {

            $scope.data.groups[$scope.data.groups.indexOf(group)] = groupEdited;

        })
        .catch(function() {

        });

    }

    $scope.delete = function(group) {

        swal({
            title: "Estás seguro ?",
            text: "El grupo ya no estará disponible",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si, eliminar",
            closeOnConfirm: false
        }, function(){

            Groups
                .delete({
                    'id_groups': group.id_groups
                })
                .then(function(response) {

                    $scope
                        .data
                        .groups
                        .splice(
                            $scope.data.groups.indexOf(group),
                            1
                        );

                    swal("Eliminado!", "El grupo ha sido eliminado.", "success");
                })

        });

    }

}
ClientsGroupsController.$inject = [
    '$scope', '$modal',
    'DTOptionsBuilder', 'DTColumnDefBuilder', 'Clients', 'Groups',
    'groups'
];
