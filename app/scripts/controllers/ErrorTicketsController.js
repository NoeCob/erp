function ErrorTicketsController(
  $scope, $rootScope, $timeout, $filter, $stateParams, $modal,
  Tickets,
  DTOptionsBuilder, DTColumnDefBuilder, errorCodes, API_AGUAGENTE, DTColumnBuilder, $compile, technicians
) {

  $scope.data = {
    'errorCodes': errorCodes,
    'lastAssignedTechnician': null,
    'selectedTicket': null,
    'searchStatus': {
      'status': ''
    },
    'technician': (function () {

      if ($stateParams.id_technicians) {

        var technician = $filter('filter')(technicians, {'id': $stateParams.id_technicians}, true)[0];

        var oFReader = new FileReader();

        oFReader.readAsDataURL(technician.image);

        oFReader.onload = function (oFREvent) {

          $timeout(function () {

            technician.imageSrc = 'data:' + technician.type + ';base64,' + oFREvent.target.result.split(',')[1];

          }, 100);

        }

        return technician;

      } else {

        return false;

      }

    })(),
    'dtInstance': {},
    'dtOptions': DTOptionsBuilder.newOptions()
      .withDOM('<"html5buttons"B>lTfgitp')
      .withButtons([{
        extend: 'excel',
        exportOptions: {
          columns: ':not(:last-child)'
        },
        text: 'Excel'
      },
        {
          extend: 'pdf',
          exportOptions: {
            columns: ':not(:last-child)'
          },
          text: 'PDF',
          title: 'Contratos'
        },
        {
          extend: 'print',
          exportOptions: {
            columns: ':not(:last-child)'
          },
          text: 'Imprimir',
          title: 'Contratos',
          customize: function (win) {
            $(win.document.body).addClass('white-bg');
            $(win.document.body).css('font-size', '10px');

            $(win.document.body).find('table')
              .addClass('compact')
              .css('font-size', 'inherit');
          }
        }
      ])
      .withOption('order', [])
      .withOption('createdRow', function (row, data, dataIndex) {
        $timeout(function () {
          $compile(angular.element(row).contents())($scope)
        });
      })
      .withOption('ajax', {
        url: API_AGUAGENTE.URL + '/tickets/get_tickets',
        type: 'GET',
        data: function (d) {
          d.status = $scope.data.searchStatus.status;
        }
      })
      .withDataProp('data')
      .withOption('processing', true)
      .withOption('serverSide', true)
      .withPaginationType('full_numbers'),
    'dtColumns': [
      DTColumnBuilder.newColumn('id_tickets').withTitle('Estatus').renderWith(function (data, type, ticket) {
        //JSON.stringify(client).replace(/'/g, '&#39')
        var _html = "<h2>#" + ticket.id_tickets + "</h2>" +
          "<i class='fa fa-info-circle fa-lg text-success' style='cursor:pointer;' popover-placement='bottom' popover-template='\"ticket-details.html\"' popover-append-to-body='true' popover-trigger='click' ng-click='selectTicket(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'></i>" +
          "<br/><br/>";

        console.log(ticket.status);
        switch (ticket.status) {
          case 'opened':
            _html = _html + "<span class='label label-danger'><span>Abierto</span></span>";
            break;
          case 'assigned':
            _html = _html + "<span class='label label-warning'><span>Asignado</span></span>";
            break;
          case 'completed':
            _html = _html + "<span class='label label-info'><span>Completado por el técnico</span></span>";
            break;
          case 'confirmed':
            _html = _html + "<span class='label label-success'><span>Firmado por el cliente</span></span>";
            break;
          case 'closed':
            _html = _html + "<span class='label label-success'><span>Cerrado</span></span>";
            break;
        }

        if (ticket.status != 'opened') {
          _html += "<p style='margin-top:10px;cursor:pointer;' popover-placement='bottom' popover-template='\"last-assigned-technician.html\"' popover-title='Datos del técnico' popover-append-to-body='true' popover-trigger='click' ng-click='getLastAssignedTechnician(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><i class='fa fa-truck fa-2x faa-passing-reverse " + (ticket.status == 'assigned' ? 'animated faa-slow' : null) + "'></i></p>";
        }

        return _html;
      }),
      DTColumnBuilder.newColumn('client').withTitle('Cliente').renderWith(function (data, type, ticket) {
        var _html = '<a eat-click>' + ticket.client.name + '</a><h4>' + ticket.error_code.category + ' - ' + ticket.error_code.code + ' - ' + ticket.error_code.name + '</h4>';

        switch (ticket.type) {
          case 'installation':
            _html += '<h5><b>Instalación</b></h5>';
            break;
          case 'installation':
            _html += '<h5><b>Llamada de soporte</b></h5>';
            break;
          case 'installation':
            _html += '<h5><b>Cambio de filtro</b></h5>';
            break;
          case 'installation':
            _html += '<h5><b>Desinstalación</b></h5>';
            break;
          case 'installation':
            _html += '<h5><b>Cambio de dirección del cliente</b></h5>';
            break;
        }

        _html += '<small>' + ticket.description + '</small>'
        return _html;
      }),
      DTColumnBuilder.newColumn('contact').withTitle('Contacto').renderWith(function (data, type, ticket) {

        var _html = '<p><i class="fa fa-home fa-fw" aria-hidden="true"></i><span style="font-size:11px;">{{ticket.client.address}} #' + ticket.client.outdoor_number +
          '<br/>' + ticket.client.colony + ' ' + ticket.client.postal_code + '<br/><b>ENTRE</b><br/>' + ticket.client.between_streets + '</span></p>' +
          '<p><i class="fa fa-envelope fa-fw" aria-hidden="true"></i><span style="font-size:11px;">' + ticket.client.email + '</span></p>' +
          '<p><span style="font-size:11px;"><i class="fa fa-phone fa-fw" aria-hidden="true"></i>' + ticket.client.phone + '</span></p>';

        return _html;
      }),
      DTColumnBuilder.newColumn('created_at').withTitle('Fecha de creación').renderWith(function (data, type, ticket) {
        var _html = "{{'" + ticket.created_at + "' | moment:'utc' | moment: 'toDate' | moment: 'format': 'ddd DD MMM YYYY HH:mm:ss'}}";
        return _html;
      }),
      DTColumnBuilder.newColumn(null).withTitle('Acciones').withOption("searchable", false).withOption("orderable", false).renderWith(function (data, type, ticket) {
        var _html = "<div class='btn-group btn-group-xs' dropdown><button type='button' class='btn btn-primary' dropdown-toggle>Opciones&nbsp;<span class='fa fa-fw fa-caret-down'></span></button>" +
          "<ul class='dropdown-menu dropdown-menu-right' role='menu' aria-labelledby='split-button'>" +
          "<li role='menuitem'><a href='' ng-click='reviewHistory(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><i class='fa fa-search'></i> Historial</a></li>" +
          "<li class='divider'></li><li role='menuitem'><a href='' ng-click='edit(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><i class='fa fa-pencil'></i> Editar</a></li><li class='divider'></li>";

        switch (ticket.status) {
          case 'confirmed':
            _html += "<li role='menuitem' ng-click='setClosed(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><a href=''>Cerrar ticket</a></li>"
            break;

          case 'completed':
            _html += "<li role='menuitem' ng-click='sendSignEmail(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><a href=''>Reenviar correo de firma al cliente</a></li>" +
              "<li role='menuitem' ng-click='forceSign(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><a href=''>Forzar la firma del ticket</a></li>";
            break;

          case 'opened':
            _html += "<li role='menuitem' ng-click='assign(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><a href=''><i class='fa fa-hand-o-right'></i> Asignar ticket</a></li>"
            break;

          case 'assigned':
            _html += "<li role='menuitem' ng-click='reassign(" + JSON.stringify(ticket).replace(/'/g, '&#39') + ")'><a href=''><i class='fa fa-hand-o-right'></i> Reasignar ticket</a></li>"
            break;

        }


        _html += "</ul>"
        return _html;
      })
    ]
  }

  $scope.assign = function (ticket) {

    ErrorTicketsController.assign(ticket);

  }

  $scope.reassign = function (ticket) {

    ErrorTicketsController.assign(ticket);

  }

  $scope.reviewHistory = function (ticket) {

    $modal.open({

      templateUrl: 'views/error-tickets-history.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        ticket: function () {

          return ticket;

        },

        history: ['Tickets', function () {

          return Tickets
            .getHistory({
              'id_tickets': ticket.id_tickets
            })
            .then(function (response) {

              var history = [];

              _ticket = angular.copy(ticket);

              _ticket.created = ticket.id_tickets;

              history = history
                .concat([
                  _ticket
                ])
                .concat((function () {

                  for (var i in response.assignations) {

                    response.assignations[i].technician.imageSrc = 'data:' + response.assignations[i].technician.type + ';base64,' + response.assignations[i].technician.image;
                  }

                  return response.assignations;

                })())
                .concat((function () {

                  for (var i in response.completions) {

                    if (response.completions[i].latitude) {

                      response.completions[i].map_url =
                        "https://maps.googleapis.com/maps/api/staticmap?" +
                        "center=" + response.completions[i].latitude + "," + response.completions[i].longitude + "&" +
                        "zoom=15&" +
                        "size=2000x200&" +
                        "scale=2&" +
                        "maptype=roadmap&" +
                        "markers=" + response.completions[i].latitude + "," + response.completions[i].longitude;

                    }

                  }

                  return response.completions;

                })())
                .concat(response.confirmations);

              if (_ticket.id_closed_by !== null) {

                __ticket = angular.copy(ticket);

                __ticket.closed = ticket.id_tickets;
                __ticket.created_at = __ticket.closed_at;

                history = history
                  .concat([__ticket]);

              }

              return history;

            })

        }]

      },
      controller: ['$filter', '$modalInstance', 'ticket', 'history',

        function ($filter, $modalInstance, ticket, history) {

          var instance = this;

          instance.data = {
            'ticket': ticket,
            'history': history
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.dateDiff = function (date) {

            if ((days = moment().diff(moment.utc(date).toDate(), 'days')) > 0)
              return days + ' dias';
            else if ((hours = moment().diff(moment.utc(date).toDate(), 'hours')) > 0)
              return hours + ' horas';
            else if ((minutes = moment().diff(moment.utc(date).toDate(), 'minutes')) > 0)
              return minutes + ' minutos';
            else if ((seconds = moment().diff(moment.utc(date).toDate(), 'seconds')) > 0)
              return seconds + ' segundos';

          }

        }
      ]
    })
      .result.then(function () {

    })
      .catch(function () {
      });

  }

  $scope.sendSignEmail = function (ticket) {

    Tickets
      .sendSignEmail(ticket)
      .then(function (response) {
        swal('Correcto!', 'Email reenviado correctamente', 'success');
      })

  }

  $scope.forceSign = function (ticket) {

    swal({
      title: "Detalles",
      text: "Especifica el porque se decidió cerrar el ticket por el cliente.",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      allowOutsideClick: false,
      confirmButtonText: "Continuar",
      confirmButtonColor: "#23c6c8",
      cancelButtonText: "Cancelar",
      animation: "slide-from-top",
    }, function (details) {

      if (details === false) return false;

      if (details.trim() === "") {
        swal.showInputError("Debes especificar detalles");
        return false;
      }

      Tickets
        .forceSign({
          'id_tickets': ticket.id_tickets,
          'client_comments': details
        })
        .then(function (response) {

          ticket.status = "confirmed";
          swal({
            title: "Operación realizada correctamente",
            text: "",
            type: "success",
            confirmButtonText: "Continuar",
            confirmButtonColor: "#2196F3",
            allowOutsideClick: false,
          }, function () {

            swal.close();

          });

        });

    });

  }

  $scope.setClosed = function (ticket) {

    $modal.open({

      templateUrl: 'views/error-tickets-close.html',
      controllerAs: 'instance',
      size: 'sm',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        ticket: function () {

          return angular.copy(ticket);

        }

      },
      controller: [

        '$scope', '$filter', '$modalInstance', '$timeout',
        'Tickets',
        'ticket',

        function (
          $scope, $filter, $modalInstance, $timeout,
          Tickets,
          ticket) {

          var instance = this;
          ticket.final_service_fee = parseFloat(ticket.final_service_fee);
          instance.data = {
            'ticket': ticket,
            'loading': false
          }

          instance.close = function (form, ticket) {

            if (form.$valid) {

              swal({
                title: "Estás seguro de cerrar el ticket y efectuar el cobro al cliente ?",
                type: "warning",
                showCancelButton: true,
                allowOutsideClick: false,
                confirmButtonColor: "#128f76",
                confirmButtonText: "Si, continuar.",
                closeOnConfirm: false
              }, function () {


                Tickets
                  .setStatus({
                    'id_tickets': ticket.id_tickets,
                    'status': 'closed',
                    'final_service_fee': ticket.final_service_fee,
                    'final_service_fee_reasons': ticket.final_service_fee_reasons
                  })
                  .then(function (response) {

                    swal({
                      title: "Operación realizada correctamente",
                      text: "",
                      type: "success",
                      confirmButtonText: "Continuar",
                      confirmButtonColor: "#2196F3",
                      allowOutsideClick: false,
                    }, function () {

                      instance.data.loading = false;
                      swal.close();
                      $modalInstance.close(response);

                    });

                  });


              });
            } else {

              $scope.$broadcast('show-errors-check-validity');

              swal({
                title: 'Un momento',
                text: "Favor de completar los datos obligatorios.",
                type: "error",
                confirmButtonColor: "#128f76",
                confirmButtonText: "Aceptar"
              });

            }

          }

          instance.dismiss = function () {
            $modalInstance.dismiss();
          }

        }
      ]
    })
      .result.then(function (closedTicket) {

      $scope.data.filteredTickets[$scope.data.tickets.indexOf(ticket)] = closedTicket;

    })
      .catch(function () {
      });

  }

  $scope.getLastAssignedTechnician = function (ticket) {

    if ($scope.data.lastAssignedTechnician === null) {

      Tickets
        .getAssignations({
          'id_tickets': ticket.id_tickets
        })
        .then(function (response) {

          var assignation = $filter('orderBy')(response, 'created_at', true)[0];

          var oFReader = new FileReader();

          oFReader.readAsDataURL(assignation.technician.image);

          oFReader.onload = function (oFREvent) {

            $timeout(function () {

              assignation.technician.imageSrc = 'data:' + assignation.technician.type + ';base64,' + oFREvent.target.result.split(',')[1];
              $scope.data.lastAssignedTechnician = assignation;

            }, 100);

          };

        })


    } else {

      $scope.data.lastAssignedTechnician = null;

    }

  }

  $scope.selectTicket = function (ticket) {

    if ($scope.data.selectedTicket === null) {

      $scope.data.selectedTicket = ticket;

    } else {

      $scope.data.selectedTicket = null;

    }

  }

  $scope.dateDiff = function (date) {

    return moment().diff(moment.utc(date).toDate(), 'hours', true).toFixed(2) + ' Horas';

  }

  $scope.filterTickets = function (status) {
    $scope.data.searchStatus.status = status;
    $scope.data.dtInstance.reloadData(function () {

    }, false);
  }

  ErrorTicketsController.assign = function (ticket) {

    $modal.open({

      templateUrl: 'views/error-tickets-assign.html',
      controllerAs: 'instance',
      size: 'lg',
      backdrop: 'static',
      keyboard: false,
      resolve: {

        ticket: function () {

          return ticket;

        },

        technicians: function () {

          return technicians;

        }

      },
      controller: [

        '$filter', '$modalInstance', '$timeout',
        'DTOptionsBuilder', 'DTColumnDefBuilder',
        'ticket', 'technicians',

        function (
          $filter, $modalInstance, $timeout,
          DTOptionsBuilder, DTColumnDefBuilder,
          ticket, technicians) {

          var instance = this;

          instance.data = {
            'ticket': ticket,
            'technicians': technicians,
            'selectedTechnician': null,
            'dtOptions': DTOptionsBuilder.newOptions()
              .withDOM('<"html5buttons"B>lTfgitp')
              .withButtons([{
                extend: 'excel',
                exportOptions: {
                  columns: ':not(:last-child)'
                },
                text: 'Excel'
              },
                {
                  extend: 'pdf',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'PDF',
                  title: 'Contratos'
                },
                {
                  extend: 'print',
                  exportOptions: {
                    columns: ':not(:last-child)'
                  },
                  text: 'Imprimir',
                  title: 'Contratos',
                  customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                      .addClass('compact')
                      .css('font-size', 'inherit');
                  }
                }
              ])
              .withOption('order', []),
            'dtColumnDefs': [
              DTColumnDefBuilder.newColumnDef(3).withOption("type", "date-nl")
            ],
            'loading': false
          }

          instance.close = function () {

            $modalInstance.dismiss();

          }

          instance.selectTechnician = function (technician) {

            instance.data.selectedTechnician = technician;

            var oFReader = new FileReader();

            oFReader.readAsDataURL(technician.image);

            oFReader.onload = function (oFREvent) {

              $timeout(function () {

                technician.imageSrc = 'data:' + technician.type + ';base64,' + oFREvent.target.result.split(',')[1];

              }, 100);

            };

          }

          instance.assign = function (ticket, technician) {

            swal({
              title: "Instrucciones",
              text: "Proporciona más detalles al técnico para un servico más rápido y eficiente.",
              type: "input",
              showCancelButton: true,
              closeOnConfirm: false,
              allowOutsideClick: false,
              confirmButtonText: "Continuar",
              confirmButtonColor: "#23c6c8",
              cancelButtonText: "Cancelar",
              animation: "slide-from-top",
            }, function (details) {

              if (details === false) return false;

              if (details.trim() === "") {
                swal.showInputError("Debes especificar instrucciones");
                return false;
              }

              instance.data.loading = true;

              Tickets
                .setStatus({
                  'id_tickets': ticket.id_tickets,
                  'status': 'assigned',
                  'id_technicians': technician.id,
                  'details': details
                })
                .then(function (response) {

                  swal({
                    title: "Operación realizada correctamente",
                    text: "",
                    type: "success",
                    confirmButtonText: "Continuar",
                    confirmButtonColor: "#2196F3",
                    allowOutsideClick: false,
                  }, function () {

                    instance.data.loading = false;
                    swal.close();
                    $modalInstance.close(response);

                  });

                });

            });
          }

        }
      ]
    })
      .result.then(function (assignedTicket) {

      $scope.data.filteredTickets[$scope.data.tickets.indexOf(ticket)] = assignedTicket;

    })
      .catch(function () {
      });

  }

  //$scope.reviewHistory($filter('filter')(tickets, {'id_tickets': 18})[0]);

}

ErrorTicketsController.$inject = [
  '$scope', '$rootScope', '$timeout', '$filter', '$stateParams', '$modal',
  'Tickets',
  'DTOptionsBuilder', 'DTColumnDefBuilder', 'errorCodes', 'API_AGUAGENTE', 'DTColumnBuilder', '$compile', 'technicians'
];
