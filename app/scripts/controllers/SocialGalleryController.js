function SocialGalleryController(
	$scope, $modal, $timeout,
	DTOptionsBuilder, DTColumnDefBuilder, Posts,
	posts) {

	$scope.data = {
		'posts' : posts,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([])
            .withOption('order', [])
    }

    $scope.add = function() {

        $modal.open({

            templateUrl: 'views/posts-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'Posts',

                function($scope, $filter, $modalInstance, Posts) {

                    var instance = this;

                    instance.data = {
                        'post': {
                            'title': '',
                            'text': '',
                            'image': ''
                        },
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, post) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Posts
                                .create({
                                    'title': post.title,
                                    'text': post.text,
                                    'image': post.image
                                })
                                .then(function(response) {

                                    swal({
                                        title: 'Post creado correctamente',
                                        text: '',
                                        type: 'success',
                                        confirmButtonColor: '#128f76',
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);

                                    });

                                })


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

					instance.loadImage = function(elemento) {
						var oFReader = new FileReader();
						oFReader.readAsDataURL(document.getElementById(elemento.id).files[0]);
						oFReader.onload = function(oFREvent) {
							$scope.$apply(function() {
								instance.data.post.image = oFREvent.target.result;
							});
						};
					}

					instance.removeImage = function() {
						instance.data.post.image = '';
    				}

                }
            ]
        })
        .result.then(function (post) {

            $scope.data.posts.push(post);

        })
        .catch(function() {

        });

    }

    $scope.edit = function(post) {

        $modal.open({

            templateUrl: 'views/posts-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'Posts',

                function($scope, $filter, $modalInstance, Posts) {

                    var instance = this;

                    instance.data = {
                        'post': angular.copy(post),
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, post) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Posts
                                .update(post)
                                .then(function(response) {

                                    swal({
                                        title: 'Post actualizado correctamente',
                                        text: '',
                                        type: 'success',
                                        confirmButtonColor: '#128f76',
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);

                                    });

                                });


                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

					instance.loadImage = function(elemento) {
						var oFReader = new FileReader();
						oFReader.readAsDataURL(document.getElementById(elemento.id).files[0]);
						oFReader.onload = function(oFREvent) {
							$scope.$apply(function() {
								instance.data.post.image = oFREvent.target.result;
							});
						};
					}

					instance.removeImage = function() {
						instance.data.post.image = '';
    				}

                }
            ]
        })
        .result.then(function (editedPost) {

            $scope.data.posts[$scope.data.posts.indexOf(post)] = editedPost;

        })
        .catch(function() {

        });

    }

    $scope.delete = function(post) {

        swal({
            title: '¿Estás seguro de eliminar el código de error?',
            text: '',
            type: 'warning',
            confirmButtonColor: '#128f76',
            confirmButtonText: 'Si, eliminar.',
            cancelButtonText: 'Cancelar',
            showCancelButton: true
        }, function() {

            Posts
                .delete({id_posts:post.id_posts})
                .then(function () {

                    $scope.data.posts.splice(
                        $scope.data.posts.indexOf(post),
                        1
                    );

                })

        });

    }

	$scope.reviewImage = function(post) {

        post.loading = false;
		$('#blueimp-gallery').data('use-bootstrap-modal', false);
		blueimp.Gallery(
			[{
				'href': post.image
			}],
			$('#blueimp-gallery').data()
		);

    }

}
SocialGalleryController.$inject = [
	'$scope', '$modal', '$timeout',
	'DTOptionsBuilder', 'DTColumnDefBuilder', 'Posts',
	'posts'
];
