function SocialResponsabilityController($scope, $modal, Dashboard, Withdrawals) {

    $scope.data = {
        "withdrawals": [],
        "balance": 0
    };

    Dashboard.get().then(function(dashboard) {
        $scope.data.balance = dashboard.totals.client_social +
                              dashboard.totals.aguagente_social -
                              dashboard.withdrawals.reduce(function(total, w) { return total + parseFloat(w.amount); }, 0);

    });

    Withdrawals.getAll().then(
        function(withdrawals) {
            $scope.data.withdrawals = withdrawals;
        }
    );

    $scope.openWithdrawal = function(withdrawal) {

        $modal.open({

            templateUrl: 'views/withdrawals.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            controller: ['$modalInstance', 'Withdrawals',

                function($modalInstance, Withdrawals) {

                    var instance = this;

                    instance.data = {
                        'loading': false,
                        'withdrawal': {
                            'amount': 0,
                            'date': '',
                            'reason': ''
                        },
                        'dpOpened': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.withdraw = function(form, withdrawal) {

                        if(form.$valid) {

                            instance.data.loading = true;

                            Withdrawals
                                .create(withdrawal)
                                .then(function(w) {

                                    $scope.data.withdrawals.push(w);
                                    $scope.data.balance = $scope.data.balance - w.amount;
                                    instance.data.loading = false;
                                    $modalInstance.close();

                                }, function() {

                                    instance.data.loading = false;

                                });

                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",
                                type: "error",
                                confirmButtonColor: "#128f76",
                                confirmButtonText: "Aceptar"
                            });

                        }

                    }

                }

            ]

        })

    }

}

SocialResponsabilityController.$inject = ['$scope', '$modal', 'Dashboard', 'Withdrawals'];
