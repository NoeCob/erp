function ForgotPasswordController($scope, AuthService) {

	$scope.data = {
		'email': '',
		'loading' : false
	}

	$scope.requestResetPassword = function(form, data) {

		if(form.$valid) {

			$scope.data.loading = true;

			AuthService
	            .sendInstructions({
	                'email': data.email
	            })
	            .then(function(response) {

                	$scope.data.loading = false;

	                swal({
		                title: 'Correcto!',
		                html: true,
		                text: 'las instrucciones para reestablecer tu contraseña fueron enviadas al correo electrónico: <b>'+data.email+'</b>',   
		                type: 'success',    
		                confirmButtonColor: '#128f76',   
		                confirmButtonText: 'Aceptar'
		            });

	            })

		} else {

			$scope.$broadcast('show-errors-check-validity');

            swal({
                title: 'Un momento',
                text: 'Favor de completar los datos obligatorios.',   
                type: 'error',    
                confirmButtonColor: '#128f76',   
                confirmButtonText: 'Aceptar'
            });

		}

	}

}

ForgotPasswordController.$inject = ['$scope', 'AuthService'];