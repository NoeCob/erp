function ErrorTicketsCompleteController (
	$scope, $timeout, $state,
	FileUploader, Tickets, ResizeFileImage,
	assignation, errorCodes, position
) {

	$scope.data = {
		'assignation': (function() {

			var oFReader = new FileReader();

	        oFReader.readAsDataURL(assignation.technician.image);

	        oFReader.onload = function (oFREvent) {

		        $timeout(function(){

		        	assignation.technician.imageSrc = 'data:' + assignation.technician.type + ';base64,' + oFREvent.target.result.split(',')[1];

		        }, 100);

	        };

	        return assignation;

		})(),
		'errorCodes': errorCodes,
		'completion': {
			'id_tickets': assignation.ticket.id_tickets,
			'id_assignations': assignation.id_assignations,
			'error_code': '',
			'extra_charges': '',
			'extra_charges_reasons': '',
            'serial_number': assignation.ticket.client.serial_number,
			'recipient': '',
			'work_description': '',
			'tds_in': '',
			'tds_out': '',
			'used_parts': [{
				part: ''
			}],
			'position': (function(){

				if(position._status) {

					position.map_url =
					"https://maps.googleapis.com/maps/api/staticmap?"+
					"center="+position.position.coords.latitude+","+position.position.coords.longitude+"&"+
					"zoom=15&"+
					"size=2000x150&"+
					"scale=2&"+
					"maptype=roadmap&"+
					"markers="+position.position.coords.latitude+","+position.position.coords.longitude;

					return position;

				} else  {

					swal(
						'No se pudo acceder a tu ubicación',
						position.error.code === 1 ? 'Proporciona permisos al navegador para acceder a tu ubicación' : '',
						'warning'
					);

					return position;

				}

			})()
		},
	 	'loading': false,
	 	'finishedJob': false
	}

	$scope.uploader = new FileUploader({
        url: ''
    });

    var filterImage = {
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    var failedImage = function(item, filter, options) {
        swal(
            "Error!",
            "El archivo "+item.name+" no es una imagen",
            "error"
        );

    };

    $scope.uploader.filters.push(filterImage);
    $scope.uploader.onWhenAddingFileFailed = failedImage;
    $scope.uploader.onAfterAddingFile = function(fileItem) {

        ResizeFileImage.photo(fileItem._file, 1200, 'file', function(file) {
            $scope.uploader.queue[$scope.uploader.getIndexOfItem(fileItem)]._file = file;
        });

    };

    $scope.uploader.onAfterAddingAll = function(items) {

        //console.log($scope.uploader);
    };


    $scope.addressUploader = new FileUploader({
        url: '',
        alias: 'address'
    });

    $scope.addressUploader.filters.push(filterImage);
    $scope.addressUploader.onWhenAddingFileFailed = failedImage;
    $scope.addressUploader.onAfterAddingFile = function(fileItem) {

        ResizeFileImage.photo(fileItem._file, 1200, 'file', function(file) {
            $scope.addressUploader.queue[$scope.addressUploader.getIndexOfItem(fileItem)]._file = file;
        });

    };


    $scope.thereAreExtraCharges = function(extra_charges) {

    	return angular.isNumber(extra_charges);

    }

    $scope.addPart = function() {

    	$scope.data.completion.used_parts.push({
    		part: ''
    	});

    }

    $scope.removePart = function(part) {

    	$scope.data.completion.used_parts.splice(
            $scope.data.completion.used_parts.indexOf(part),
            1
        );

    }

    $scope.completeTicket = function(form, completion) {
        
        //console.log(form.$error);
    	if(form.$valid && angular.isObject(completion.error_code)) {

            $scope.data.loading = true;

            Tickets
                .setStatus({
                	'id_tickets': completion.id_tickets,
                	'id_assignations': completion.id_assignations,
                	'id_error_codes' : completion.error_code.id_error_codes,
                	'latitude': completion.position._status ? completion.position.position.coords.latitude : null,
                	'longitude': completion.position._status ? completion.position.position.coords.longitude : null,
					'extra_charges' : completion.extra_charges,
					'extra_charges_reasons' : completion.extra_charges_reasons,
					'recipient' : completion.recipient,
                    'serial_number': completion.serial_number,
					'work_description': completion.work_description,
					'tds_in' : completion.tds_in,
					'tds_out' : completion.tds_out,
					'used_parts' : completion.used_parts.map(function (part) { return part.part; }).join(','),
                    'photos' :  $scope.uploader.queue.map(function(item) {return item._file}),
                    'proof_address': $scope.addressUploader.queue.length ? $scope.addressUploader.queue[0]._file : '',
					'status': 'completed'
                })
                .then(function (response) {

                    $scope.data.loading = false;
                    $scope.data.finishedJob = true;

                    swal({
                        title: "Correcto!",
                        text: "gracias, tu trabajo ha concluido",
                        type: "success",
                        confirmButtonText: "Continuar",
                        confirmButtonColor: "#2196F3",
                        allowOutsideClick: true
                    });

                }, function(response) {
                	$scope.data.loading = false;
                });


        } else {

            if(!angular.isObject(completion.error_code))
                $scope.data.completion.error_code = '';

            $scope.$broadcast('show-errors-check-validity');

            swal({
                title: 'Un momento',
                text: "Favor de completar los datos obligatorios.",
                type: "error",
                confirmButtonColor: "#128f76",
                confirmButtonText: "Aceptar"
            });

        }

    }

    ResizeFileImage.init();

}
ErrorTicketsCompleteController.$inject = [
	'$scope', '$timeout', '$state',
	'FileUploader', 'Tickets', 'ResizeFileImage',
	'assignation', 'errorCodes', 'position'
];
