function FinanceClientsController($scope, $modal, Debts, Charges, SubscriptionEvents, clients) {

	$scope.data = {
		'clients' : clients
    }

    $scope.showDebts = function(client) {

        $modal.open({

            templateUrl: 'views/debts.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            controller: ['$modalInstance', '$timeout', 'Debts', 'Charges', 'SubscriptionEvents', 'PushNotifications',

                function($modalInstance, $timeout, Debts, Charges, SubscriptionEvents, PushNotifications) {

                    var instance = this;

                    instance.data = {
                        'loading': false,
						'message': '',
						'client': client,
						'debts': [],
						'charges': [],
						'subscription_events': []
                    };

					Debts.getAll({"id_clients":client.id_clients, 'status':'unpaid'})
						.then(function(debts){
							instance.data.debts=debts;
						});
					Charges.getAll({"id_clients":client.id_clients})
						.then(function(charges){
							instance.data.charges=charges;
					});
					SubscriptionEvents.getAll({"id_clients":client.id_clients})
						.then(function(subscription_events){
							instance.data.subscription_events=subscription_events;
						});

					instance.tipos = {
						'subscription.canceled': 'Cancelación',
						'subscription.paused': 'Pausa',
						'subscription.resumed': 'Reanudación',
						'subscription.created': 'Creación',
						'subscription.paid': 'Pago recibido',
						'subscription.payment_failed': 'Pago fallido'
					};

					instance.estatus =  {
						'in_trial': 'Periodo de prueba',
						'active': 'Activa',
						'paused': 'Pausada',
						'past_due': 'Fallando',
						'canceled': 'Cancelada',
					};

                    instance.close = function() {
                        $modalInstance.dismiss();
                    }

					instance.notifyUser = function() {
						instance.data.loading = true;
						PushNotifications
							.notifyUser(instance.data.message, [client.id_users])
							.then(function() {
								swal({
									title: 'Notificación enviada',
									type: 'success',
									confirmButtonColor: '#128f76',
									confirmButtonText: 'Aceptar'
								});
								instance.data.loading = false;
								instance.data.message = '';
							});
					}

                    instance.charge = function(debt) {

                        instance.data.loading = true;

                        Debts
                            .charge(debt.id_debts)
                            .then(function(d) {

								client.debt -= d.amount + d.collection_fees + d.moratory_fees;
								instance.data.debts.splice(instance.data.debts.indexOf(debt), 1);
								instance.data.loading = false;

								$timeout(function() {
									Charges
										.getAll({"id_clients":client.id_clients})
										.then(function(charges){instance.data.charges=charges;});
								}, 5000);

								swal({
									title: 'Cargo realizado satisfactoriamente',
									type: 'success',
									confirmButtonColor: '#128f76',
									confirmButtonText: 'Aceptar'
								});

                            }, function(response) {

								instance.data.loading = false;

							});

					}

					instance.getCharge = function(id_charges) {
						for(var i in instance.data.charges)
							if(instance.data.charges[i].id_charges == id_charges)
								return instance.data.charges[i];
						return null;
					}

				}
            ]
        })

    }

}

FinanceClientsController.$inject = [
	'$scope', '$modal', 'Debts', 'Charges', 'SubscriptionEvents', 'clients'
];
