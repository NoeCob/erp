function TechniciansController(
	$scope, $modal, $timeout,
	DTOptionsBuilder, DTColumnDefBuilder, Users, 
	technicians) {

	$scope.data = {
		'technicians' : technicians,
		'dtOptions': DTOptionsBuilder.newOptions()
	        .withDOM('<"html5buttons"B>lTfgitp')
	        .withButtons([
	            {
	            	extend: 'excel',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Excel'
	            },
	            {
	            	extend: 'pdf', 
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'PDF',
                    title: 'Técnicos'
	            },
	            {
	            	extend: 'print',
	            	exportOptions: {
			          columns: ':not(:last-child)'
			        },
			        text: 'Imprimir',
                    title: 'Técnicos',
	                customize: function (win){
	                    $(win.document.body).addClass('white-bg');
	                    $(win.document.body).css('font-size', '10px');

	                    $(win.document.body).find('table')
	                        .addClass('compact')
	                        .css('font-size', 'inherit');
	                }
	            }
	        ])
            .withOption('order', []),
		'dtColumnDefs': [
	        DTColumnDefBuilder.newColumnDef(4).notSortable(),
            DTColumnDefBuilder.newColumnDef(2).withOption("type", "date-nl")
	    ]
    }

    $scope.add = function() {

        $modal.open({

            templateUrl: 'views/technicians-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'AuthService', 
            
                function($scope, $filter, $modalInstance, AuthService) {

                    var instance = this;

                    instance.data = {
                        'user': {
                            'name': '',
                            'contractor': '',
                            'email': '',
                            'phone': '',
                            'image': '',
                            'password': '',
                            'checkPassword': ''
                        },
                        'loading': false
                    }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, user) {

                        if(form.$valid && user.password === user.checkPassword && user.password.length >= 6) {

                            instance.data.loading = true;

                            AuthService
                                .register({
                                	'role': 'Technician',		
                                    'name': user.name,
                                    'contractor': user.contractor,
                                    'email': user.email,
                                    'phone': user.phone,
                                    'image': user.image,
                                    'password': user.password
                                })
                                .then(function(response) {
                                    instance.data.loading = false;
                                    $modalInstance.close(response);
                                })

                            
                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",   
                                type: "error",    
                                confirmButtonColor: "#128f76",   
                                confirmButtonText: "Aceptar"
                            });
                            
                        }

                    }

                    instance.previewImage = function(elemento){

				        var oFReader = new FileReader();
				        oFReader.readAsDataURL(document.getElementById(elemento.id).files[0]);

				        oFReader.onload = function (oFREvent) {

				            var img = new Image();

				            img.onload = function(){
 
				                var canvas = document.getElementById('element-image');
				                var ctx = canvas.getContext("2d");
				                ctx.clearRect(0, 0, canvas.width, canvas.height);
				                canvas.width = img.width;
				                canvas.height = img.height;
				                ctx.drawImage(img, 0, 0, img.width, img.height);
				                
				            };

				            img.src = oFREvent.target.result;

				        };
				    
				    }

				    instance.removeImage = function(canvas_id) {
				        
				        var canvas = document.getElementById('element-image');
				        var ctx = canvas.getContext("2d");
				        ctx.clearRect(0, 0, canvas.width, canvas.height);

    				}

                }
            ]
        })
        .result.then(function (administrator) {
            
            $scope.data.technicians.push(administrator);

        })
        .catch(function() {
        
        });

    }
    
    $scope.toogleStatus = function($index,technician){
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#F44336",
                confirmButtonText: "Yes, Sure !",
                closeOnConfirm: true
            }, function () {
                              Users.toogleStatus(technician)
                              $scope.data.technicians[$index].status =  !technician.status;
                              //return !technician.status;
            });
    }


    $scope.edit = function(technician) {

        $modal.open({

            templateUrl: 'views/technicians-save.html',
            controllerAs: 'instance',
            size: 'lg',
            backdrop: 'static',
            keyboard: false,
            resolve: { },
            controller: ['$scope', '$filter', '$modalInstance', 'Users', 
            
                function($scope, $filter, $modalInstance, Users) {

                    var instance = this;

                    technician.password = '';
                    technician.checkPassword = '';

                    instance.data = {
                        'user': technician,
                        'loading': false
                    }

                    var oFReader = new FileReader();
			        oFReader.readAsDataURL(technician.image);

			        oFReader.onload = function (oFREvent) {

			            var img = new Image();

			            img.onload = function(){

			                var canvas = document.getElementById('element-image');
			                var ctx = canvas.getContext("2d");
			                ctx.clearRect(0, 0, canvas.width, canvas.height);
			                canvas.width = img.width;
			                canvas.height = img.height;
			                ctx.drawImage(img, 0, 0, img.width, img.height);
			                
			            };

			            img.src = oFREvent.target.result;

			        }

                    instance.close = function() {

                        $modalInstance.dismiss();

                    }

                    instance.save = function(form, user) {

                        if(form.$valid && user.password === user.checkPassword && user.password.length >= 6) {

                            instance.data.loading = true;

                            Users
                                .update({
                                    'id': user.id,
                                    'name': user.name,
                                    'contractor': user.contractor,
                                    'email': user.email,
                                    'phone': user.phone,
                                    'image': user.image,
                                    'password': user.password
                                })
                                .then(function(response) {

                                    swal({
                                        title: 'Técnico actualizado correctamente',
                                        text: '',   
                                        type: 'success',    
                                        confirmButtonColor: '#128f76',   
                                        confirmButtonText: 'Aceptar'
                                    }, function() {

                                        instance.data.loading = false;
                                        $modalInstance.close(response);
                                        
                                    });

                                })

                            
                        } else {

                            $scope.$broadcast('show-errors-check-validity');

                            swal({
                                title: 'Un momento',
                                text: "Favor de completar los datos obligatorios.",   
                                type: "error",    
                                confirmButtonColor: "#128f76",   
                                confirmButtonText: "Aceptar"
                            });
                            
                        }

                    }


                    instance.previewImage = function(elemento){

				        var oFReader = new FileReader();
				        console.log(document.getElementById(elemento.id).files[0]);
				        oFReader.readAsDataURL(document.getElementById(elemento.id).files[0]);

				        oFReader.onload = function (oFREvent) {

				            var img = new Image();

				            img.onload = function(){
 
				                var canvas = document.getElementById('element-image');
				                var ctx = canvas.getContext("2d");
				                ctx.clearRect(0, 0, canvas.width, canvas.height);
				                canvas.width = img.width;
				                canvas.height = img.height;
				                ctx.drawImage(img, 0, 0, img.width, img.height);
				                
				            };

				            img.src = oFREvent.target.result;

				        };
				    
				    }

				    instance.removeImage = function(canvas_id) {
				        
				        var canvas = document.getElementById('element-image');
				        var ctx = canvas.getContext("2d");
				        ctx.clearRect(0, 0, canvas.width, canvas.height);

    				}

                }
            ]
        })
        .result.then(function (editedTechnician) {
            
            $scope.data.technicians[$scope.data.technicians.indexOf(technician)] = editedTechnician;

        })
        .catch(function() {
        
        });

    }

	$scope.reviewImage = function(technician) {

		var oFReader = new FileReader();

        oFReader.readAsDataURL(technician.image);

        oFReader.onload = function (oFREvent) {

	        $timeout(function(){

	            blueimp.Gallery( 
	                [{
	                	'href': 'data:'+technician.type+';base64,'+oFREvent.target.result.split(',')[1]
	                }],
	                $('#blueimp-gallery').data()
	            );
	            
	        }, 250);

        };

    }

}
TechniciansController.$inject = [
	'$scope', '$modal', '$timeout',
	'DTOptionsBuilder', 'DTColumnDefBuilder', 'Users', 
	'technicians'
];
