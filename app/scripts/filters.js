angular
    .module('aguagente-front-filters', [])
    .filter('moment', momentFilter)
    .filter('secondsToDate', secondsToDate)
