/**
 * DebtsService
 * Modela la interacción con los deudos
 *
 * @param $http  ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, charge: charge}}
 * @constructor
 */
function DebtsService ($http, API_AGUAGENTE) {

	return  {

    /**
     * getAll
     * Devuelve todas las deudas usando filtros.
     * @param params objeto que contiene información sobre el filtrado de los datos.
     */
		getAll: function(params) {

			return $http
				.get(API_AGUAGENTE.URL + '/debts', {"params":params})
				.then(function(response) {
					var debts = response.data.response;
					for(var i in debts) {
						debts[i].amount = parseInt(debts[i].amount) / 100;
						debts[i].collection_fees = parseInt(debts[i].collection_fees) / 100;
						debts[i].moratory_fees = parseInt(debts[i].moratory_fees) / 100;
					}
					return debts;
				})

		},

    /**
     * charge
     * Intenta hacer un cargo a algún cliente por medio de CONEKTA.
     * @param id_debts ID del cliente.
     */
    charge: function(id_debts) {

			return $http
				.post(API_AGUAGENTE.URL + '/debts/' + id_debts + '/charge')
				.then(function(response) {
					var debt = response.data.response;
					debt.amount = parseInt(debt.amount) / 100;
					debt.collection_fees = parseInt(debt.collection_fees) / 100;
					debt.moratory_fees = parseInt(debt.moratory_fees) / 100;
					return debt;
				})

		}

	}

}

DebtsService.$inject = ['$http', 'API_AGUAGENTE'];
