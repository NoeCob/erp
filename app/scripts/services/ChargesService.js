/**
 * ChargesService
 * Servicio encargado de recuperar los cargos echos a cada usuario.
 *
 * @param $http  ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll}}
 * @constructor
 */
function ChargesService($http, $resource, API_AGUAGENTE) {
	var resource = $resource(
		API_AGUAGENTE.URL + '/charges/',
		{
			'id_charges': '@id_charges'
		},
		{
			getAll: {
				method: 'GET'
			},
			create: {
				method: 'POST'
			}
		}
	);

	return  {

    /**
     * Devuelve los cargos echos a cada susuario.
     * @param params
     */
		getAll: function(params) {
			params['conekta'] = 'true';
			return resource
				.getAll(params)
				.$promise
				.then(function(response) {
					//console.log(response);
					return response.response;
				})

		},
		create: function (payload) {
			return resource
				.create(payload)
				.$promise
				.then(function (response) {
					return response.response;
				});
		}

	};

}

ChargesService.$inject = ['$http', '$resource', 'API_AGUAGENTE'];
