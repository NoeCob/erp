/**
 * Elements
 * Modela el comportamiento de los elementos asociados a cada categoría en la sección *extras* del apartado *contratos*.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getByCategory: getByCategory, create: create, update: update, delete: delete}}
 * @constructor
 */
function Elements ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/elements/:id_categories_elements',
		{
			id_categories_elements: '@id_categories_elements'
		},
		{
			getByCategory: {
				method: 'GET'
			},

			create: {
				method: 'POST',
				headers: {
					'Content-Type': undefined
				},
				transformRequest: function(data) {

					if(typeof data.image === 'string') delete data.image;

            		var formData = new FormData();

					for (var key in data){

				   		formData.append(key, data[key]);

					}

					return formData;

				}
			},

			update: {
				method: 'POST',
				headers: {
					'Content-Type': undefined
				},
				transformRequest: function(data) {

					if(typeof data.image === 'string') delete data.image;

            		var formData = new FormData();

					formData.append('_method', 'PUT');

					for (var key in data){

				   		formData.append(key, data[key]);

					}


					return formData;

				}
			},

			delete: {
				method: 'DELETE'
			}
		}
	);

	return  {

    /**
     * getByCategory
     * Devuelve los elementos de cada categoría.
     */
		getByCategory: function() {

			return resource
				.getAll()
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * create
     * Crea un nuevo elemento.
     * @param payload objeto con información del nuevo elemento.
     */
		create: function(payload) {

			return resource
				.create(payload)
				.$promise
				.then(function (response) {

					var element = response.response;

					if(element.image) {

						var byteCharacters = atob(element.image);

						var byteNumbers = new Array(byteCharacters.length);

						for (var k = 0; k < byteCharacters.length; k++) {

						    byteNumbers[k] = byteCharacters.charCodeAt(k);

						}
						var byteArray = new Uint8Array(byteNumbers);

						var blob = new Blob(
							[byteArray], {
							type: element.type,
							encoding: 'utf-8'
						});

						element.image = new File(
							[blob],
							'image'
						);

					}

					return element;

				});

		},

    /**
     * update
     * Actualiza la información de un Elemento.
     * @param payload objeto con la información del elemento a actualizar.
     */
		update: function(payload) {

			return resource
				.update(payload)
				.$promise
				.then(function (response) {

					var element = response.response;

					if(element.image) {

						var byteCharacters = atob(element.image);

						var byteNumbers = new Array(byteCharacters.length);

						for (var k = 0; k < byteCharacters.length; k++) {

						    byteNumbers[k] = byteCharacters.charCodeAt(k);

						}

						element.image = new Blob(
							[(new Uint8Array(byteNumbers)).buffer], {
							type: element.type,
							encoding: 'utf-8'
						});

					}

					return element;

				});

		},

    /**
     * delete
     * Elimina algún elemento.
     * @param params información sobre el Elemento a eliminar.
     */
		delete: function(params) {

			return resource
				.delete(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		}

	};

}

Elements.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
