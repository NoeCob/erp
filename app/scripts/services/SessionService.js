/**
 * SessionService
 * Acciones del usuario con relación a la sesión.
 *
 * @param $window ng/window
 * @param $http ng/http
 * @param ROLES
 * @constructor
 */
var SessionService = function ($window, $http, ROLES) {

    this.user = null;

    /**
     * setUser
     * Establece el usuario en el localStorage.
     *
     * @param user Usuario
     */
    this.setUser = function (user) {

        this.user = user;

        $window.localStorage['aguagente-front'] = JSON.stringify(user);

    };

    /**
     * getUser
     * Devuelve el usuario de la sesión activa
     *
     * @returns {null}
     */
    this.getUser = function () {

          if( angular.isObject(this.user) ) {

              return this.user;

          } else if( angular.isDefined($window.localStorage['aguagente-front']) && $window.localStorage['aguagente-front'] !== 'undefined') {

              this.user = angular.fromJson($window.localStorage['aguagente-front']);

              return this.user;

          } else {

              return null;

          }

      };


  /**
   * destroy
   * Elimina al usuario de la sesión activa
   */
  this.destroy = function() {

        delete this.user;
        delete $window.localStorage['aguagente-front'];

    };

};

SessionService.$inject = ['$window', '$http', 'ROLES'];
