/**
 * Cards
 * Servicio utilizado para obtener información relacionada a las tarjetas de crédito de cada usuario.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll}}
 * @constructor
 */
function Cards ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/clients/:id_clients/cards/:id_cards',
		{
			'id_clients': '@id_clients',
			'id_cards': '@id_cards'
		},
		{
			getAll: {
				method: 'GET'
			}
		}
	);

	return  {

    /**
     * getAll
     * obtiene todas las tarjetas de crédito del usuario.
     * @param params objeto envíado para la recuperación de la info relacionada a las tarjetas de crédito del usuario.
     */
		getAll: function(params) {

			/*return $q(function(resolve, reject){

				resolve([{
                    "id": "card_GH5k4s1ixhPf9nqE",
                    "created_at": 1457727132,
                    "active": true,
                    "last4": "1881",
                    "object": "card",
                    "exp_month": "12",
                    "exp_year": "19",
                    "brand": "VISA",
                    "name": "Jorge Lopez",
                    "customer_id": "cus_S1NPcHumiPYFR8Egd",
                    "default": true
                }, {
                    "id": "card_Cer2yjCNdH9AAqLK",
                    "created_at": 1458332617,
                    "active": true,
                    "last4": "4242",
                    "object": "card",
                    "exp_month": "07",
                    "exp_year": "17",
                    "brand": "VISA",
                    "name": "Rafael Arizaga",
                    "customer_id": "cus_S1NPcHumiPYFR8Egd",
                    "default": false
                }]);

			});*/

			return resource
				.getAll(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		}

	}

}

Cards.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
