/**
 * PushNotifications
 * Se encarga de la ejecución de tareas relacionadas a las notificaciones push.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $http  ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{notifyUser: notifyUser, notifyGroup: notifyGroup, notifyRole: notifyRole}}
 * @constructor
 */
function PushNotifications ($resource, $http, API_AGUAGENTE) {

	return  {

    /**
     * notifyUser
     * Inicia el proceso para enviar notificaciones ***push*** a clientes.
     * @param message Mensaje a ser enviado.
     * @param users Lista de usuarios a los que se les envíara el mensaje.
     */
		notifyUser: function(message, users) {

			return $http.post(
				API_AGUAGENTE.URL + '/notifyUser',
				{
					'message': message,
					'users': users
				}
			);

		},

    /**
     * notifyGroup
     * Inicia el proceso para enviar notificaciones ***push*** a grupos de usuarios.
     * @param message Mensaje a ser enviado.
     * @param groups Lista de grupos a los que se les enviará el mensaje.
     */
		notifyGroup: function(message, groups) {

			return $http.post(
				API_AGUAGENTE.URL + '/notifyGroup',
				{
					'message': message,
					'groups': groups
				}
			);

		},

    /**
     * notifyRole
     * Inicia el proceso para enviar notificaciones ***push*** a usuarios según su rol.
     *
     * @param message Mensaje a ser enviado.
     * @param roles Lista de roles a los que se les enviará el mensaje.
     */
		notifyRole: function(message, roles) {

			return $http.post(
				API_AGUAGENTE.URL + '/notifyAll',
				{
					'message':message,
					'roles':roles
				}
			);

		}

	}

}

PushNotifications.$inject = ['$resource', '$http', 'API_AGUAGENTE'];
