/**
 * Roles
 * Devuelve los roles de usuario.
 *
 * @param $http ng/http
 * @param API_AGUAGENTE Constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll}}
 * @constructor
 */
function Roles ($http, API_AGUAGENTE) {

	return  {

		getAll: function() {

			return $http
				.get(API_AGUAGENTE.URL + '/roles')
				.then(function(response) {
					return response.data.response;
				})

		}

	}

}

Roles.$inject = ['$http', 'API_AGUAGENTE'];
