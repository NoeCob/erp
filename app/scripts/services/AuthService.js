/**
 * Service used to authorize access to API
 *
 * @param $http  ng/http
 * @param SessionService
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getToken: getToken, register: register, login: login, logout: logout, sendInstructions: sendInstructions, resetPassword: resetPassword, isAuthenticated: isAuthenticated, isAuthorized: isAuthorized}}
 * @constructor
 */
var AuthService = function ($http, SessionService, API_AGUAGENTE) {

    var authService = {};

    return {
      /**
       * getToken
       * returns Devuelve el token de autorización
       */
        getToken: function() {

            return $http
                .get(API_AGUAGENTE.URL + '/auth/token')
                .then(function (response) {

                    $http.defaults.headers.common['X-CSRF-TOKEN'] = response.data.token;

                }, function(response) {

                    return response;

                });

        },

      /**
       * register
       * registra cualqueir tipo de usuario en la base de datos de aguagente
       * @param payload objeto enviado vía POST, al servidor, para registrar al usuario.
       */
      register: function(payload) {

            return $http({
                method: 'POST',
                url: API_AGUAGENTE.URL+'/auth/register',
                headers: {
                    'Content-Type': undefined
                },
                data: payload,
                transformRequest: function(data) {

                    if(typeof data.image === 'string') delete data.image;

                    var formData = new FormData();

                    for (var key in data){

                        formData.append(key, data[key]);

                    }

                    return formData;

                }
            })
            .then(function (response) {

                var technician = angular.fromJson(response.data).user;

                if(technician && technician.image) {

                    var byteCharacters = atob(technician.image);

                    var byteNumbers = new Array(byteCharacters.length);

                    for (var k = 0; k < byteCharacters.length; k++) {

                        byteNumbers[k] = byteCharacters.charCodeAt(k);

                    }

                    technician.image = new Blob(
                        [(new Uint8Array(byteNumbers)).buffer], {
                        type: technician.type,
                        encoding: 'utf-8'
                    });

                }

                return technician;

            }, function (response) {
                return response;

            });

        },

        /**
         * login
         * logea al usuario en el sistema
         * @param credentials array con credenciales necesarias para logueo
         */
        login: function (credentials) {

            return $http

            .post(API_AGUAGENTE.URL+'/auth/login', credentials)
            .then(function (response) {

                SessionService.setUser(response.data.user);

                return response;

            }, function (response) {

                return response;

            });

        },

      /**
       * logout
       * cierra sesión del usuario
       */
      logout: function () {

            return $http

            .get(API_AGUAGENTE.URL+'/auth/logout')
            .then(function (response) {
                return response;
            }, function (response) {
                return response;
            });

        },

      /**
       * sendInstructions
       * envía las instrucciones para iniciar sesión
       * @param payload
       */
        sendInstructions: function(payload) {

            return $http

            .post(API_AGUAGENTE.URL+'/auth/email', payload)
            .then(function (response) {

                return response;

            }, function (response) {

                return response;

            });

        },

      /**
       * resetPassword
       * Inicia el proceso para reseteo de password del usuario
       * @param payload
       */
      resetPassword: function(payload) {

            return $http

            .post(API_AGUAGENTE.URL+'/auth/reset', payload)
            .then(function (response) {

                return response;

            }, function (response) {

                return response;

            });

        },

      /**
       * isAuthenticated
       * verifica que el usuario esté autenticado
       * @returns {boolean}
       */
        isAuthenticated: function () {

            return angular.isObject(SessionService.getUser());

        },

      /**
       * isAuthorized
       * verifica si el usuario está autorizado para efectuar alguna acción
       * @param authorizedRoles objeto con los datos sobre los roles autorizados.
       * @returns {*|boolean}
       */
        isAuthorized: function (authorizedRoles) {

            //algunas vistas requieren login pero no roles
            if(authorizedRoles.length === 0) {

                return authService.isAuthenticated();

            }

            authorizedRoles = authorizedRoles.reduce(function(a, b) {
                return a + b;
            });

            return (authService.isAuthenticated() && (authorizedRoles & SessionService.user.rol) !== 0);

        }

    }

}

AuthService.$inject = ['$http', 'SessionService', 'API_AGUAGENTE'];
