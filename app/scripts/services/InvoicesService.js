function Invoices($resource, $q, $filter, API_AGUAGENTE) {

  var resource = $resource(
    API_AGUAGENTE.URL + '/clients/:id_invoices', {}, {
      send_invoice: {
        method: 'POST',
        url: API_AGUAGENTE.URL + '/invoices/send_invoice/:id_invoices'
      },
      download_files: {
        method: 'GET',
        url: API_AGUAGENTE.URL + '/invoices/download_files/:id_invoices'
      },
    }
  );

  return {
    send_invoice: function (params) {

      return resource
        .send_invoice(params)
        .$promise
        .then(function (response) {
          return response.response;
        });

    },
    download_files: function (params) {

      return resource
        .download_files(params)
        .$promise
        .then(function (response) {
          return response.response;
        });

    },

  };

}

Invoices.$inject = ['$resource', '$q', '$filter', 'API_AGUAGENTE'];
