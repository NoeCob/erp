/**
 * SuscriptionEventsService
 * Devuelve todas las suscriciones después de haber aplicado algunos filtros.
 *
 * @param $http ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 *
 * @returns {{getAll: getAll}}
 * @constructor
 */
function SubscriptionEventsService ($http, API_AGUAGENTE) {

	return  {

    /**
     * getAll
     * Deuvelve todas las suscripciones después de haber aplicado algunos filtros.
     * @param params
     */
		getAll: function(params) {

			return $http
				.get(API_AGUAGENTE.URL + '/subscription-events', {'params': params})
				.then(function(response) {
					return response.data.response;
				});

		}

	};

}

SubscriptionEventsService.$inject = ['$http', 'API_AGUAGENTE'];
