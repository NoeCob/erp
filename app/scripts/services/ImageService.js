/**
 * Imagen
 * Contiene utilidades para el manejo de imágenes.
 *
 * @param $http ng/http
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @returns {{urlToBase64: urlToBase64, base64Image: base64Image, base64ToBlob: base64ToBlob, blobImage: blobImage, urlToBlob: urlToBlob}}
 * @constructor
 */
function Imagen ($http,$q) {


	return  {

    /**
     * urlToBase64
     * Devuelve una imagen a partir de su URL.
     * @param imgUrl URL de la imagen.
     */
		urlToBase64 : function(imgUrl){

			var defer = $q.defer();
			var img = new Image();
      img.setAttribute('crossOrigin', 'anonymous');
      img.onload = function() {
				var canvas = document.createElement('canvas');
				canvas.width = img.width;
				canvas.height = img.height;
				var ctx = canvas.getContext('2d');
				ctx.drawImage(img, 0, 0);
				var dataURL = canvas.toDataURL('image/jpeg');
				defer.resolve(dataURL);
      };
      img.src = imgUrl;
      return defer.promise;

		},

    /**
     * base64Image
     * Devuelve una imagen en base64 y la muestra en un canvas.
     *
     * @param img Imagen a mostrar en canvas.
     * @returns {string}
     */
		base64Image : function(img){

			img.setAttribute('crossOrigin', 'anonymous');
			var canvas = document.createElement('canvas');
			canvas.width = img.width;
			canvas.height = img.height;
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0);
			var dataURL = canvas.toDataURL('image/png');
			return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');

		},

    /**
     * base64ToBlob
     * Devuelve una imagen en formato Blob.
     *
     * @param dataURI URI con la imagen en base64.
     * @returns {*} Objeto con la imagen en formato Blobl || Null.
     */
		base64ToBlob : function(dataURI){
			if( !angular.isString(dataURI) ) return null;
			var byteString;
			if(dataURI.split(',')[0].indexOf('base64') >= 0) byteString = atob(dataURI.split(',')[1]);
			else byteString = unescape(dataURI.split(',')[1]);
			var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
			var ia = new Uint8Array(byteString.length);
			for (var j = 0; j < byteString.length; j++)
				ia[j] = byteString.charCodeAt(j);

			return {
				'file' : new Blob([ia], {type:mimeString}),
				'mime' : mimeString
			};
		},

    /**
     * blobImage
     * Devuelve una imagen en formato Blobl y la dibuja en un canvas.
     * @param img Imagen a dibujar.
     */
		blobImage : function(img){
			img.setAttribute('crossOrigin', 'anonymous');
			var canvas = document.createElement('canvas');
			canvas.width = img.width;
			canvas.height = img.height;
			var ctx = canvas.getContext('2d');
			ctx.drawImage(img, 0, 0);
			return canvas.toBlob(function(blob){
				return blob;
			});
		},

    /**
     * urlToBlob
     * Devuelve una imagen en formato Blob a partir de su URL.
     * @param imageUrl URL de la imagen.
     */
		urlToBlob : function(imageUrl){

			return  $http({
				        url: imageUrl,
				        method: 'GET',
				        responseType: 'blob',
				        withCredentials : false,
				        headers : {
				        	'X-CSRF-TOKEN': undefined
				        }
				    })
					.then(function(response){
						return response.data;
					});
		}

	};

}

Imagen.$inject = ['$http','$q'];
