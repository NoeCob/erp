/**
 * Tickets
 * Modela la interacción de los tickets del sistema.
 *
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create, update: update, setStatus: setStatus, sendSignEmail: sendSignEmail, forceSign: forceSign, getAssignations: getAssignations, getAssignation: getAssignation, getHistory: getHistory}}
 * @constructor
 */
function Tickets ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/tickets/:id_tickets',
		{
			id_tickets: '@id_tickets'
		},
		{
			getAll: {
				method: 'GET'
			},
			create: {
				method: 'POST'
			},
			update: {
				method: 'PUT'
			},
			setStatus: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/tickets/:id_tickets/setStatus',
				headers: {
                    'Content-Type': undefined
                },
				transformRequest: function(data) {

                    var formData = new FormData();

                    for (var key in data){

                    	if(key === 'photos') {

                    		for(var index in data[key]) {

                    			formData.append('photos[]', data[key][index]);

                    		}

                    	}
                    	else {

                        	formData.append(key, data[key]);

                    	}

                    }

                    return formData;

                }
			},
			forceSign: {
				method: 'POST',
				url: API_AGUAGENTE.URL + '/confirmations'
			},
			sendSignEmail: {
				method: 'POST',
				url: API_AGUAGENTE.URL+'/tickets/:id_tickets/send-mail'
			},
			getAssignations: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/assignations'
			},
			getAssignation: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/assignations/:id_assignations'
			},
			getCompletions: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/completions'
			},
			getConfirmations: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/confirmations'
			}
		}
	);

	return  {

    /**
     * getAll
     * Devuelve todos los tickets usando filtros.
     * @param params
     */
		getAll: function(params) {

			return resource
				.get(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * create
     * Genera un nuevo ticket en la base de datos y envía un email al cliente.
     *
     * @param payload
     */
		create: function(payload) {

			/*return $q(function (resolve, reject) {
				resolve(payload);
			});*/
			return resource
				.create(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * update
     * Modifica la información de un ticket pormedio de su ID.
     *
     * @param params
     */
		update: function(params) {

			return resource
				.update(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * setStatus
     * Establece el status de un ticket por medio de su ID.
     *
     * @param params
     */
		setStatus: function(params) {

			/*return $q(function(resolve, reject) {
				resolve({
					'id_tickets': 1,
					'created_at': '2016-05-25 21:31:53',
					'id_clients': 7,
					'id_error_codes': 1,
					'error_code': {
						active: "1",
						created_at:"2016-05-24 17:23:56",
						name: "Client Error",
						category: "Basic Codes",
						code: "10",
						updated_at: "2016-05-24 17:23:56"
					},
					'description': 'Lorem ipsum dolor sit amet, facilisi interdum ornare, elit adipiscing viverra interdum torquent tellus eget, tincidunt in. Tincidunt erat ut ducimus nulla dis, dapibus et sit enim justo et elit, est dui at molestie hac, orci pharetra felis eros. Aute urna proin vehicula vitae. Et tincidunt donec massa leo quam, habitasse lacinia imperdiet in eget ultrices, ut faucibus justo, sit in wisi a nunc vestibulum mi',
					'status': params.status,
					'updated_at': '2016-05-25 21:54:25',
					'client': {
				      "id_clients": "7",
				      "name": "Celia Leticia Zambrano Ambrosio",
				      "rfc": null,
				      "address": "Paseo del Mirador 65",
				      "outdoor_number": "0",
				      "inside_number": null,
				      "phone": "3331270350",
				      "email": "lettyzambrano1@gmail.com",
				      "between_streets": "Paseo del Lago y Paseo del Prado",
				      "colony": "La Floresta",
				      "postal_code": "45920",
				      "state": "Jalisco",
				      "county": "Ajijic, Chapala",
				      "level": "VIAJERO",
				      "device_id": null,
				      "conekta_token": "cus_49r7zEsqCLsM1dCHV",
				      "referred_by": "10",
				      "status": "accepted",
				      "created_at": "2016-04-29 13:52:34",
				      "updated_at": "2016-05-11 16:42:41",
				      "id_users": "7",
				      "id_groups": "1",
				      "installation_fee": 250,
				      "monthly_fee": 199,
				      "deposit": 500,
				      "social_responsability": "1",
				      "signed_in": "1",
				      "group": {
				        "id_groups": "1",
				        "name": "Aguagratis",
				        "installation_fee": 249,
				        "monthly_fee": 199,
				        "sign_into": {
				          "id_groups": "1",
				          "name": "Aguagratis",
				          "installation_fee": "24900.00",
				          "monthly_fee": "19900.00",
				          "sign_into": "1",
				          "created_at": "2016-05-13 02:39:32",
				          "updated_at": "2016-05-20 02:37:05",
				          "deposit": "50000.00",
				          "plan_id": "plan_yDkTZ4GEMnMr1hg1",
				          "social_plan_id": "plan_ERmJgsPKZGnBFQJq"
				        },
				        "created_at": "2016-05-13 02:39:32",
				        "updated_at": "2016-05-20 02:37:05",
				        "deposit": 500,
				        "plan_id": "plan_yDkTZ4GEMnMr1hg1",
				        "social_plan_id": "plan_ERmJgsPKZGnBFQJq"
				      },
				      "contract": {
				        "id_contracts": "7",
				        "id_clients": "7",
				        "created_at": "2016-04-29 14:34:01",
				        "updated_at": "2016-05-19 01:24:59",
				        "paid_at": "2016-05-19 01:24:55",
				        "_status": 1
				      }
				    }
				});
			});*/
			return resource
				.setStatus(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * sendSignEmail
     * Envía el correo de firma del cliente.
     *
     * @param params
     */
		sendSignEmail: function(params) {

			return resource
				.sendSignEmail(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * forceSign
     * Forza el envío del correo de la firma del cliente.
     *
     * @param params
     */
		forceSign: function(params) {

			return resource
				.forceSign(params)
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * getAssignations
     * Devuelve los tickets que ya han sido asignados.
     *
     * @param params
     */
		getAssignations: function(params) {

			return resource
				.getAssignations(params)
				.$promise
				.then(function (response) {

					var assignations = response.response;

					for(var i in assignations) {

						var byteCharacters = atob(assignations[i].technician.image);

						var byteNumbers = new Array(byteCharacters.length);

						for (var k = 0; k < byteCharacters.length; k++) {

						    byteNumbers[k] = byteCharacters.charCodeAt(k);

						}


						assignations[i].technician.image = new Blob(
							[(new Uint8Array(byteNumbers)).buffer], {
							type: assignations[i].technician.type,
							encoding: 'utf-8'
						});

					}

					return assignations;

				});

		},

    /**
     * getAssignation
     * Devuelve un ticket asignado usando filtros.
     *
     * @param params
     */
		getAssignation: function(params) {

			return resource
				.getAssignation(params)
				.$promise
				.then(function (response) {

					var assignation = response.response;

					var byteCharacters = atob(assignation.technician.image);

					var byteNumbers = new Array(byteCharacters.length);

					for (var k = 0; k < byteCharacters.length; k++) {

					    byteNumbers[k] = byteCharacters.charCodeAt(k);

					}

					assignation.technician.image = new Blob(
						[(new Uint8Array(byteNumbers)).buffer], {
						type: assignation.technician.type,
						encoding: 'utf-8'
					});


					return assignation;

				});

		},

    /**
     * getHistory
     * Devuelv eel historial de un ticket
     *
     * @param params
     */
		getHistory: function(params) {

			return $q.all({
				'assignations': resource
					.getAssignations(params)
					.$promise
					.then(function (response) {

						return response.response;
					}),
				'completions': resource
					.getCompletions(params)
					.$promise
					.then(function (response) {
						return response.response;
					}),
				'confirmations': resource
					.getConfirmations(params)
					.$promise
					.then(function (response) {
						return response.response;
					})
			});

		}

	};

}

Tickets.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
