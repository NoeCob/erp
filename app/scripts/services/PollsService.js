/**
 * Polls
 * Devuelve información relacionada a las encuestas.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create, update: update, get: get, delete: delete, getAnswers: getAnswers}}
 * @constructor
 */
function Polls ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL + '/polls/:id_polls',
		{
			'id_polls': '@id_polls'
		},
		{
			getAll: {
				method: 'GET'
			},
			get: {
				method: 'GET'
			},
			create: {
				method: 'POST'
			},
			update: {
				method: 'PUT'
			},
			delete: {
				method: 'DELETE'
			},
			getAnswers: {
				method: 'GET',
				url: API_AGUAGENTE.URL + '/answers'
			}
		}
	);

	return  {

    /**
     * getAll
     * Devuelve todas las encuestas.
     */
		getAll: function() {

			return resource
				.getAll()
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * create
     * Crea una nueva encuesta.
     * @param payload
     */
		create: function(payload) {

			return resource
				.create(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});
		},

    /**
     * update
     * Actualiza la información de una encuesta tras haberla editado.
     * @param payload ID de la encuesta
     */
		update: function(payload) {

			return resource
				.update(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * get
     * Muestra una encuesta por medio de su ID.
     * @param params ID de la encuesta.
     */
		get: function(params) {

			return resource
				.get({
					'id_polls': params.id_polls
				})
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * delete
     * Elimina una encuesta por medio de su ID.
     * @param params ID de la encuesta.
     */
		delete: function(params) {

			return resource
				.delete({
					'id_polls': params.id_polls
				})
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * getAnswers
     * Devuelve respuestas usando filtros.
     * @param poll
     */
		getAnswers: function(poll) {

			return resource
				.getAnswers({
					'poll': poll.name
				})
				.$promise
				.then(function (response) {

					return response.response;

				});
		}

	};

}

Polls.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
