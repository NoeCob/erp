/**
 * DashboardService
 * Despliega el dashboard
 *
 * @param $http ng/http
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{get: get}}
 * @constructor
 */
function DashboardService ($http, API_AGUAGENTE) {

	return  {

    /**
     * get
     * Devuelve los datos a desplegar en el dashboard usando filtros
     * @param params objeto que contiene información sobre el filtrado de los datos.
     */
		get: function(params) {
			return $http
				.get(API_AGUAGENTE.URL + '/dashboard', {'params': params})
				.then(function(response) {
					return response.data;
				});

		}

	};

}

DashboardService.$inject = ['$http', 'API_AGUAGENTE'];
