/**
 * ErrorCodes
 * Modela la interacción con los códigos de Error.
 *
 * @param $resource ngResource/resource A factory which creates a resource object that lets you interact with RESTful server-side data sources.
 * @param $q ng/q A service that helps you run functions asynchronously, and use their return values (or exceptions) when they are done processing.
 * @param API_AGUAGENTE constante que contiene info relacionada la URL de la API
 * @returns {{getAll: getAll, create: create, update: update, delete: delete}}
 * @constructor
 */
function ErrorCodes ($resource, $q, API_AGUAGENTE) {

	var resource = $resource(
		API_AGUAGENTE.URL+'/error-codes/:id_error_codes',
		{
			id_error_codes: '@id_error_codes'
		},
		{
			getAll: {
				method: 'GET'
			},

			create: {
				method: 'POST'
			},

			update: {
				method: 'PUT'
			},

			delete: {
				method: 'DELETE'
			}
		}
	);

	return  {

    /**
     * getAll
     * Devuelve todos los códigos de error.
     */
		getAll: function () {

			return resource
				.getAll()
				.$promise
				.then(function (response) {
					return response.response;
				});

		},

    /**
     * create
     * Crea un código de error
     * @param payload información relacionada al nuevo código de error.
     */
		create: function(payload) {

			return resource
				.create(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * update
     * Actualiza la información de algún código de error.
     * @param payload información relacionada al código de error a actualizar.
     */
		update: function(payload) {

			return resource
				.update(payload)
				.$promise
				.then(function (response) {

					return response.response;

				});

		},

    /**
     * delete
     * Elimina algún código de error.
     * @param params información relacionada al código de error a eliminar.
     */
		delete: function(params) {

			return resource
				.delete(params)
				.$promise
				.then(function (response) {

					return response.response;

				});

		}

	};

}

ErrorCodes.$inject = ['$resource', '$q', 'API_AGUAGENTE'];
