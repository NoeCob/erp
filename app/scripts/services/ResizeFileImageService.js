/**
 * ResizeFileImage
 * Devuelve una imagen
 *
 *
 * @returns {{init: init, photo: photo, resize: resize, output: output}}
 * @constructor
 */
function ResizeFileImage() {

	return {

    /**
     * init
     * Inicializa el valor asignado a la calidad de la imagen
     * @param outputQuality Calidad de la imágen. (Valor entre 0 y 1)
     */
		init: function(outputQuality) {

			this.outputQuality = outputQuality ? outputQuality : 0.8;

		},

    /**
     * photo
     * devuelve una imagen en un canvas definiéndole un tamaño máximo
     *
     * @param file Dirección de la imagen.
     * @param maxSize Tamaño máximo de la imagen.
     * @param outputType Tipo del archivo. 'file' | 'dataURL'
     * @param callback
     */
		photo: function(file, maxSize, outputType, callback) {

			var _this = this;

			var reader = new FileReader();
			reader.readAsDataURL(file);

			reader.onload = function (readerEvent) {
				_this.resize(readerEvent.target.result, file, maxSize, outputType, callback);
			};

		},

    /**
     * resize
     * Cambia el tamaño de una imagen.
     *
     * @param dataURL
     * @param file
     * @param maxSize Tamaño máximo de la imagen.
     * @param outputType
     * @param callback
     */
		resize: function(dataURL, file, maxSize, outputType, callback) {
			var _this = this;

			var image = new Image();
			image.onload = function (imageEvent) {

				// Resize image
				var canvas = document.createElement('canvas'),
					width = image.width,
					height = image.height;
				if (width > height) {
					if (width > maxSize) {
						height *= maxSize / width;
						width = maxSize;
					}
				}
				else {
					if (height > maxSize) {
						width *= maxSize / height;
						height = maxSize;
					}
				}
				canvas.width = width;
				canvas.height = height;
				canvas.getContext('2d').drawImage(image, 0, 0, width, height);

				_this.output(canvas, file, outputType, callback);

			};
			image.src = dataURL;

		},

    /**
     * output
     * Muestra una imagen en un canvas, respetando un tamaño máximo.
     *
     * @param canvas
     * @param file Imagen a mostrar.
     * @param outputType Tipo de imagen {'file | 'dataURL'}
     * @param callback
     */
		output: function(canvas, file, outputType, callback) {

			switch (outputType) {

				case 'file':
					canvas.toBlob(function (blob) {
						callback(blob);
					}, file.type, this.outputQuality);
					break;

				case 'dataURL':
					callback(canvas.toDataURL(file.type, this.outputQuality));
					break;

			}

		}

	};
}
