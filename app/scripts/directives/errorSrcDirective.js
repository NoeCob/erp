function errorSrc () {

    return {

        link: function(scope, element, attrs) {

            element.bind('error', function() {

                $(this)
                    .css('visibility', 'hidden')
                    .parents('.parent')
                    .after()
                    .html('<span class="fa-stack fa-2x">\
                              <i class="fa fa-file-image-o fa-stack-1x text-primary"></i>\
                              <i class="fa fa-ban fa-stack-2x text-danger"></i>\
                            </span>\
                            <span class="h3 text-primary">No se encontró la imágen</span>');

                /*if (attrs.src != attrs.errSrc) {

                    attrs.$set('src', attrs.errSrc);

                }*/

            });
        }

    }

}