function rotateImg() {

    return function(scope, element, attrs) {

    	$(element).on('click', function(event) {

    		if(attrs.rotateImgParentSelector) {

    			var image = $(this).parents(attrs.rotateImgParentSelector).find(attrs.rotateImgTargetSelector);

    		} else if(attrs.rotateImgTargetSelector) {

    			var image = $(attrs.rotateImgTargetSelector);
    			
    		}

			$(image).data('rotation') === undefined && $(image).data('rotation', 0);

			var rotation = $(image).data('rotation') + parseInt(attrs.rotateImgDeg);

			rotation = rotation < -360 || rotation > 360 ? parseInt(attrs.rotateImgDeg) : rotation;

			$(image).data('rotation', rotation);

            $(image).css({
                '-moz-transform': 'rotate(' + rotation + 'deg)',
                '-webkit-transform': 'rotate(' + rotation + 'deg)',
                '-o-transform': 'rotate(' + rotation + 'deg)',
                '-ms-transform': 'rotate(' + rotation + 'deg)'
            });

        });

    }
}