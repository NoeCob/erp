function MainController($scope, $rootScope, $state, Fullscreen, SessionService, AuthService, Dashboard) {

    $scope.currentUser = null;

    $rootScope.setCurrentUser = function(user) { $scope.currentUser = user; }
    $rootScope.getCurrentUser = function() { return $scope.currentUser; }


    $scope.toggleFullscreen = function() {

        if (Fullscreen.isEnabled())
            Fullscreen.cancel();
        else
            Fullscreen.all();

    }

    $scope.isFullscreen = function() {

        return Fullscreen.isEnabled();

    }

    $scope.logout = function() {

        AuthService
            .logout()
            .then(function(response) {

                delete $scope.currentUser;
                SessionService.destroy();
                $state.go('login');

            }, function(error) {


            });

    }

}

MainController.$inject = ['$scope', '$rootScope', '$state', 'Fullscreen', 'SessionService', 'AuthService', 'Dashboard'];

angular
    .module('aguagente-front-controllers', [])
    .controller('MainController', MainController)
    .controller('LoginController', LoginController)
    .controller('ForgotPasswordController', ForgotPasswordController)
    .controller('ResetPasswordController', ResetPasswordController)
    .controller('ClientsController', ClientsController)
    .controller('ClientsGroupsController', ClientsGroupsController)
    .controller('ClientsCommissionsController', ClientsCommissionsController)
    .controller('ContractsController', ContractsController)
    .controller('ContractsExtrasController', ContractsExtrasController)
    .controller('AdministratorsController', AdministratorsController)
    .controller('TechniciansController', TechniciansController)
    .controller('AssignTechniciansTicketsController', AssignTechniciansTicketsController)
    .controller('ErrorCodesController', ErrorCodesController)
    .controller('ErrorTicketsController', ErrorTicketsController)
    .controller('ErrorTicketsCompleteController', ErrorTicketsCompleteController)
    .controller('PollsController', PollsController)
    .controller('SocialGalleryController', SocialGalleryController)
    .controller('PushNotificationsController', PushNotificationsController)
    .controller('FinanceClientsController', FinanceClientsController)
    .controller('DashboardController', DashboardController)
    .controller('SocialResponsabilityController', SocialResponsabilityController)
    .controller('InvoiceController', InvoiceController)