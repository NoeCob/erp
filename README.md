# arizaga

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.

## For installing a dependency that is not on bower 

cd bower_components

git clone https://github.com/dalelotts/angular-bootstrap-datetimepicker.git
